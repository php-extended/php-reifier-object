<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Iterator;
use IteratorIterator;
use Stringable;

/**
 * ReifierIterator class file.
 * 
 * This class represents an iterator that iterates over an iterator of
 * array-based recursive data structures and returns their reified form on
 * stream.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends \IteratorIterator<integer, ?T, \Iterator<integer, ?T>>
 * @implements \Iterator<integer, ?T>
 */
class ReifierIterator extends IteratorIterator implements Iterator, Stringable
{
	
	/**
	 * The reifier that packs the values of the inner iterator.
	 * 
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The name of the class of the inner objects to reify.
	 * 
	 * @var class-string<T>
	 */
	protected string $_classname;
	
	/**
	 * Whether to use the nullable methods.
	 * 
	 * @var boolean
	 */
	protected bool $_nullable;
	
	/**
	 * Whether to use the try methods.
	 * 
	 * @var boolean
	 */
	protected bool $_tryable;
	
	/**
	 * The report to be filled.
	 * 
	 * @var ?ReifierReportInterface
	 */
	protected ?ReifierReportInterface $_report = null;
	
	/**
	 * The count of objects.
	 * 
	 * @var integer
	 */
	protected int $_count = 0;
	
	/**
	 * The current object, if any.
	 * 
	 * @var ?T
	 */
	protected $_current = null;
	
	/**
	 * Builds a new ReifierIterator with the given information.
	 * 
	 * @param ReifierInterface $reifier
	 * @param class-string<T> $classname
	 * @param Iterator<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $iterator
	 * @param boolean $nullable
	 * @param boolean $tryable
	 * @param ?ReifierReportInterface $report
	 */
	public function __construct(
		ReifierInterface $reifier,
		string $classname,
		Iterator $iterator,
		bool $nullable = false,
		bool $tryable = false,
		?ReifierReportInterface $report = null
	) {
		$this->_reifier = $reifier;
		$this->_classname = $classname;
		$this->_nullable = $nullable;
		$this->_tryable = $tryable;
		$this->_report = $report;
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		parent::__construct($iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::current()
	 * @return T
	 */
	public function current() : ?object
	{
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::next()
	 * @throws ReificationThrowable
	 */
	public function next() : void
	{
		parent::next();
		$this->_count++;
		$this->_current = $this->getCurrentObject();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::key()
	 */
	public function key() : int
	{
		return $this->_count;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::rewind()
	 * @throws ReificationThrowable
	 */
	public function rewind() : void
	{
		parent::rewind();
		$this->_count = 0;
		$this->_current = $this->getCurrentObject();
	}
	
	/**
	 * Gets the current value as parsed object.
	 *
	 * @return ?T
	 * @throws ReificationThrowable
	 */
	protected function getCurrentObject() : ?object
	{
		if(!parent::valid())
		{
			return null;
		}
		
		/** @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object> $parentcurrent */
		$parentcurrent = parent::current();
		
		// @codeCoverageIgnoreStart
		if(!\is_array($parentcurrent)) // defensive programming, should not happen
		{
			$parentcurrent = [$parentcurrent];
		}
		// @codeCoverageIgnoreEnd
		
		if(!$this->_tryable)
		{
			return $this->_nullable
				? $this->_reifier->reifyNullable($this->_classname, $parentcurrent)
				: $this->_reifier->reify($this->_classname, $parentcurrent);
		}
		
		return $this->_nullable
			? $this->_reifier->tryReifyNullable($this->_classname, $parentcurrent, $this->_report, $this->_count)
			: $this->_reifier->tryReify($this->_classname, $parentcurrent, $this->_report, $this->_count);
	}
	
}
