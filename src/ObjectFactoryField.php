<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use PhpExtended\Ensurer\EnsurerInterface;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;
use ReflectionType;
use RuntimeException;

/**
 * ObjectFactoryField class file.
 * 
 * This class is a link that transforms an object that is aleady built by adding
 * information via the public fields it holds.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends ObjectFactoryLink<T>
 */
class ObjectFactoryField extends ObjectFactoryLink
{
	
	/**
	 * The keys that are ignored by this factory.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_ignoredKeys = [];
	
	/**
	 * The public fields available in the class.
	 *
	 * @var array<string, ReflectionProperty>
	 */
	protected array $_fields = [];
	
	/**
	 * The fields and the type they should resolve to.
	 *
	 * @var array<string, ReflectionType>
	 */
	protected array $_fieldTypes = [];
	
	/**
	 * Builds a new ObjectFactoryField for the given class.
	 * 
	 * @param Reifier $reifier
	 * @param EnsurerInterface $ensurer
	 * @param ReflectionClass<T> $rclass
	 * @param ObjectFactoryLink<T> $next
	 */
	public function __construct(Reifier $reifier, EnsurerInterface $ensurer, ReflectionClass $rclass, ?ObjectFactoryLink $next = null)
	{
		parent::__construct($reifier, $ensurer, $rclass, $next);
		
		foreach($this->_rclass->getProperties(ReflectionProperty::IS_PUBLIC) as $rProp)
		{
			/** @var ReflectionProperty $rProp */
			if($rProp->isStatic())
			{
				continue;
			}
			
			$this->_fields[$rProp->getName()] = $rProp;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ObjectFactoryLink::applyTo()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function applyTo($object, array $data, int $depths, string $path, ReifierConfigurationInterface $config) : object
	{
		if(null === $object)
		{
			return parent::applyTo($object, $data, $depths, $path, $config);
		}
		
		$remainingData = [];
		
		foreach($data as $key => $value)
		{
			$key = (string) $key;
			
			if(isset($this->_ignoredKeys[$key]))
			{
				$remainingData[$key] = $value;
				
				continue;
			}
			
			/** @var ?ReflectionProperty $field */
			$field = $this->_fields[$key] ?? null;
			
			if(null === $field)
			{
				$aliasFieldName = $config->getFieldNameFromAlias($this->_rclass->getName(), $key);
				if($aliasFieldName !== $key)
				{
					$field = $this->_fields[$aliasFieldName] ?? null;
				}
			}
			
			if(null === $field)
			{
				$camelFieldName = $this->getCamelCaseName($key);
				$field = $this->_fields[$camelFieldName] ?? null;
			}
			
			if(null === $field)
			{
				$dashFieldName = $this->getDashCaseName($key);
				$field = $this->_fields[$dashFieldName] ?? null;
			}
			
			if(null === $field)
			{
				$pascalFieldName = $this->getPascalCaseName($key);
				$field = $this->_fields[$pascalFieldName] ?? null;
			}
			
			if(null === $field)
			{
				$snakeFieldName = $this->getSnakeCaseName($key);
				$field = $this->_fields[$snakeFieldName] ?? null;
			}
			
			if(null === $field)
			{
				$remainingData[$key] = $value;
				$this->_ignoredKeys[$key] = 1;
				
				continue;
			}
			
			try
			{
				try
				{
					$field->setValue($object, $this->coerceValue((string) $key, $this->resolveTypeFromField($field), $value, $depths, $path.'.'.$key, $config));
				}
				catch(ReificationException $exc)
				{
					// better justification of data and depths of the problem
					throw new ReificationException($data, $exc->getDepths(), $this->_rclass->getName(), $key, $path.'.'.$key, null, -1, $exc);
				}
			}
			catch(RuntimeException $exc)
			{
				throw new ReificationException($data, $depths, $this->_rclass->getName(), $key, $path.'.'.$key, null, -1, $exc);
			}
		}
		
		return parent::applyTo($object, $remainingData, $depths, $path, $config);
	}
	
	/**
	 * Gets the type of that is expected from the given field of class.
	 *
	 * @param ReflectionProperty $rProp
	 * @return ReflectionType
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function resolveTypeFromField(ReflectionProperty $rProp) : ReflectionType
	{
		if(isset($this->_fieldTypes[$rProp->getName()]))
		{
			return $this->_fieldTypes[$rProp->getName()];
		}
		
		$type = $rProp->getType();
		if(null !== $type)
		{
			return $this->_fieldTypes[$rProp->getName()] = $type;
		}
		
		$getterPossibleNames = [];
		
		foreach(['get', 'is', 'has'] as $prefix)
		{
			$getterPossibleNames[] = $prefix.$this->getPascalCaseName($rProp->getName());
			$getterPossibleNames[] = $prefix.'_'.$this->getCamelCaseName($rProp->getName());
			$getterPossibleNames[] = $prefix.'_'.$this->getPascalCaseName($rProp->getName());
			$getterPossibleNames[] = $prefix.'_'.$this->getSnakeCaseName($rProp->getName());
		}
		
		// https://stackoverflow.com/questions/6250226/php-get-variable-type-hint-using-reflection
		foreach($this->_rclass->getMethods(ReflectionMethod::IS_PUBLIC) as $rcMethod)
		{
			/** @var ReflectionMethod $rcMethod */
			if(0 === $rcMethod->getNumberOfParameters()
				&& \in_array($rcMethod->getName(), $getterPossibleNames, true)
			) {
				$returnType = $rcMethod->getReturnType();
				if(null !== $returnType)
				{
					return $this->_fieldTypes[$rProp->getName()] = $returnType;
				}
			}
		}
		
		// TODO preg_match the export
		// TODO if native type hint is not available, use docblocks
		
		// last resort, use name of the type
		if(\in_array($rProp->getName(), ['bool', 'boolean'], true))
		{
			return $this->_fieldTypes[$rProp->getName()] = new CustomReflectionType('bool', false);
		}
		
		if(\in_array($rProp->getName(), ['int', 'integer'], true))
		{
			return $this->_fieldTypes[$rProp->getName()] = new CustomReflectionType('int', false);
		}
		
		if(\in_array($rProp->getName(), ['float', 'double', 'flt', 'dbl', 'real'], true))
		{
			return $this->_fieldTypes[$rProp->getName()] = new CustomReflectionType('float', false);
		}
		
		if(\in_array($rProp->getName(), ['string', 'str', 'needle'], true))
		{
			return $this->_fieldTypes[$rProp->getName()] = new CustomReflectionType('string', false);
		}
		
		if(\in_array($rProp->getName(), ['array', 'list', 'map', 'haystack'], true))
		{
			return $this->_fieldTypes[$rProp->getName()] = new CustomReflectionType('array', false);
		}
		
		$message = 'Failed to guess return type for field {class}::{field}';
		$context = ['{class}' => $this->_rclass->getNamespaceName(), '{field}' => $rProp->getName()];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
}
