<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use ArrayIterator;
use LogicException;
use PhpExtended\Ensurer\EnsurerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;
use Stringable;
use Throwable;

/**
 * ObjectFactoryCtor class file.
 * 
 * This class effectively transforms one slice of the array subtree into object
 * form.
 * 
 * @author anastaszor
 * @template T of object
 * @extends ObjectFactoryLink<T>
 */
class ObjectFactoryCtor extends ObjectFactoryLink implements Stringable
{
	
	/**
	 * The constructor of those classes.
	 * 
	 * @var ?ReflectionMethod
	 */
	protected ?ReflectionMethod $_ctorMethod = null;
	
	/**
	 * The constructor args, in order.
	 * 
	 * @var array<string, ReflectionParameter>
	 */
	protected array $_ctorArgs = [];
	
	/**
	 * Builds a new ObjectFactoryCtor for the given class.
	 * 
	 * @param Reifier $reifier
	 * @param EnsurerInterface $ensurer
	 * @param ReflectionClass<T> $rclass
	 * @param ObjectFactoryLink<T> $next
	 */
	public function __construct(Reifier $reifier, EnsurerInterface $ensurer, ReflectionClass $rclass, ?ObjectFactoryLink $next = null)
	{
		parent::__construct($reifier, $ensurer, $rclass, $next);
		
		$this->_ctorMethod = $this->_rclass->getConstructor();
		
		if(null !== $this->_ctorMethod)
		{
			foreach($this->_ctorMethod->getParameters() as $rParam)
			{
				/** @var ReflectionParameter $rParam */
				$this->_ctorArgs[$this->getCamelCaseName($rParam->getName())] = $rParam;
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ObjectFactoryLink::applyTo()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function applyTo($object, array $data, int $depths, string $path, ReifierConfigurationInterface $config) : object
	{
		if(null === $this->_ctorMethod || empty($this->_ctorArgs))
		{
			try
			{
				$object = $this->_rclass->newInstance();
			}
			/** @phpstan-ignore-next-line */
			catch(Throwable $exc)
			{
				$message = 'Failed to create a {class} with constructor with no args, maybe the constructor is not public or the constructor have args that were not detected.';
				$context = ['{class}' => $this->_rclass->getName()];
				
				throw new ReificationException($data, $depths, $this->_rclass->getName(), '__construct', $path, \strtr($message, $context), -1, $exc);
			}
			
			return parent::applyTo($object, $data, $depths, $path, $config);
		}
		
		// the values present in the data not consumed by the ctor
		$afterCtor = [];
		// the values present in the data consumed by the ctor
		$preCtorArgs = [];
		
		$previousExc = null;
		
		foreach($data as $originalKey => $originalData)
		{
			$camelKey = $this->getCamelCaseName((string) $originalKey);
			$rParam = $this->_ctorArgs[$camelKey] ?? null;
			
			if(null === $rParam)
			{
				$afterCtor[$originalKey] = $originalData;
				continue;
			}
			
			try
			{
				$rType = $this->resolveTypeFromParam($this->_ctorMethod, $rParam);
			}
			catch(LogicException $exc)
			{
				$message = 'Failed to resolve type for param {name}';
				$context = ['{name}' => $rParam->getName()];
				
				throw new ReificationException($data, $depths, $this->_rclass->getName(), $rParam->getName(), $path.'.'.$rParam->getName(), \strtr($message, $context), -1, $exc);
			}
			
			try
			{
				$preCtorArgs[$rParam->getName()] = $this->coerceValue($camelKey, $rType, $originalData, $depths, $path.'.'.$rParam->getName(), $config);
			}
			catch(ReificationException $exc)
			{
				if(!$config->isFieldAllowedToFail($this->_rclass->getName(), $rParam->getName()))
				{
					throw $exc;
				}
				
				// in case the reification of this object is not
				// possible, we want to silently fallback to a
				// nullable value if possible
				$previousExc = $exc;
			}
		}
		
		// new version of the ctor args, in order this time !
		$ctorArgs = [];
		
		/** @var ReflectionParameter $rParam */
		foreach($this->_ctorArgs as $camelArgName => $rParam)
		{
			// if we already have it, skip it
			if(isset($preCtorArgs[$rParam->getName()]))
			{
				$ctorArgs[$rParam->getName()] = $preCtorArgs[$rParam->getName()];
				continue;
			}
			
			try
			{
				$rType = $this->resolveTypeFromParam($this->_ctorMethod, $rParam);
			}
			catch(LogicException $exc)
			{
				$message = 'Failed to resolve type for param {name}';
				$context = ['{name}' => $rParam->getName()];
				
				throw new ReificationException($data, $depths, $this->_rclass->getName(), $rParam->getName(), $path.'.'.$rParam->getName(), \strtr($message, $context), -1, $exc);
			}
			
			try
			{
				// use the position for arrays that have no names given (like
				// data from csv arrays via fgetcsv
				if(\array_key_exists($rParam->getPosition(), $data))
				{
					unset($afterCtor[$rParam->getPosition()]);
					$ctorArgs[$rParam->getName()] = $this->coerceValue($camelArgName, $rType, $data[$rParam->getPosition()], $depths, $path.'.'.$rParam->getName(), $config);
					continue;
				}
				
				// use array_key_exists instead of isset because there may be
				// attributes with null values
				if(\array_key_exists($camelArgName, $data))
				{
					unset($afterCtor[$camelArgName]);
					$ctorArgs[$rParam->getName()] = $this->coerceValue($camelArgName, $rType, $data[$camelArgName], $depths, $path.'.'.$rParam->getName(), $config);
					continue;
				}
				
				$alias = $config->getAliasFromFieldName($this->_rclass->getName(), $camelArgName);
				if($alias !== $camelArgName && \array_key_exists($alias, $data))
				{
					unset($afterCtor[$alias]);
					$ctorArgs[$rParam->getName()] = $this->coerceValue($alias, $rType, $data[$alias], $depths, $path.'.'.$rParam->getName(), $config);
					continue;
				}
			}
			catch(ReificationException $exc)
			{
				if(!$config->isFieldAllowedToFail($this->_rclass->getName(), $camelArgName))
				{
					throw $exc;
				}
				
				// in case the reification of this object is not
				// possible, we want to silently fallback to a
				// nullable value if possible
				$previousExc = $exc;
			}
			
			try
			{
				if($rParam->isOptional() || $rParam->isDefaultValueAvailable())
				{
					unset($afterCtor[$camelArgName]);
					$ctorArgs[$rParam->getName()] = $rParam->getDefaultValue();
					continue;
				}
			}
			// @codeCoverageIgnoreStart
			catch(ReflectionException $exc)
			// @codeCoverageIgnoreEnd
			{
				// https://www.php.net/manual/en/reflectionparameter.getdefaultvalue.php
				// Note : Due to implementation details, it is not possible
				// to get the default value of built-in functions or
				// methods of built-in classes. Trying to do this will
				// result a ReflectionException being thrown.
			}
			
			// last try for empty iterables
			if(\in_array($rType->__toString(), ['array', 'iterable'], true))
			{
				unset($afterCtor[$camelArgName]);
				$ctorArgs[$rParam->getName()] = [];
				continue;
			}
			
			if(\in_array($rType->__toString(), ['Traversable', 'Iterator', 'ArrayIterator'], true))
			{
				unset($afterCtor[$camelArgName]);
				$ctorArgs[$rParam->getName()] = new ArrayIterator();
				continue;
			}
			
			if($rParam->allowsNull())
			{
				unset($afterCtor[$camelArgName]);
				$ctorArgs[$rParam->getName()] = null;
				continue;
			}
			
			$message = 'Failed to find constructor argument "{argname}" in field list, expected "{explanation}"';
			$context = ['{argname}' => $camelArgName, '{explanation}' => $rParam->__toString()];
			
			throw new ReificationException($data, $depths, $this->_rclass->getName(), $camelArgName, $path.'.'.$camelArgName, \strtr($message, $context), -1, $previousExc);
		}
		
		try
		{
			$object = $this->_rclass->newInstanceArgs($ctorArgs);
		}
		/** @phpstan-ignore-next-line */
		catch(Throwable $exc)
		{
			$message = 'Failed to create a {class} with constructor with {k} args, maybe the constructor is not public or the constructor have args that were not detected.';
			$context = ['{k}' => \count($ctorArgs), '{class}' => $this->_rclass->getName()];
			
			throw new ReificationException($data, $depths, $this->_rclass->getName(), '__construct', $path, \strtr($message, $context), -1, $exc);
		}
		
		return parent::applyTo($object, $afterCtor, $depths, $path, $config);
	}
	
}
