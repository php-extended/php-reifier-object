<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

/**
 * ReifierReport class file.
 * 
 * This is a simple implementation of the ReifierReportInterface.
 * 
 * @author Anastaszor
 */
class ReifierReport implements ReifierReportInterface
{
	
	/**
	 * The source of the report, namely a human readable information that links
	 * to the file or other resource from which the data comes.
	 * 
	 * @var string
	 */
	protected string $_source;
	
	/**
	 * The entries.
	 * 
	 * @var array<integer, ReifierReportEntryInterface>
	 */
	protected array $_entries = [];
	
	/**
	 * Builds a new ReifierReport with the given data.
	 * 
	 * @param string $source
	 * @param array<integer, ReifierReportEntryInterface> $entries
	 */
	public function __construct(string $source, array $entries = [])
	{
		$this->_source = $source;
		$this->_entries = $entries;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : ReifierReportEntryInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return \current($this->_entries) !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_entries);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportInterface::getSource()
	 */
	public function getSource() : string
	{
		return $this->_source;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportInterface::addEntry()
	 */
	public function addEntry(ReifierReportEntryInterface $entry) : ReifierReportInterface
	{
		$this->_entries[] = $entry;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportInterface::clear()
	 */
	public function clear(?string $classname = null) : int
	{
		if(null === $classname)
		{
			$count = \count($this->_entries);
			$this->_entries = [];
			
			return $count;
		}
		
		$count = 0;
		
		/** @var ReifierReportEntryInterface $entry */
		foreach($this->_entries as $key => $entry)
		{
			if($entry->getClassname() === $classname)
			{
				unset($this->_entries[$key]);
				$count++;
			}
		}
		
		if(0 < $count)
		{
			$this->_entries = \array_values($this->_entries);
		}
		
		return $count;
	}
	
}
