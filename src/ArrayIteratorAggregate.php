<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use ArrayIterator;
use Iterator;
use IteratorAggregate;
use Stringable;

/**
 * ArrayIteratorAggregate class file.
 * 
 * This class is an \ArrayIterator that also implements the \IteratorAggregate
 * interface by delegating itself.
 * 
 * @author Anastaszor
 * @template T
 * @implements \IteratorAggregate<T>
 */
class ArrayIteratorAggregate implements IteratorAggregate, Stringable
{
	
	/**
	 * The values.
	 * 
	 * @var array<integer, T>
	 */
	protected array $_values = [];
	
	/**
	 * Builds a new ArrayIteratorAggregate with the given values.
	 * 
	 * @param array<integer, T> $values
	 */
	public function __construct(array $values = [])
	{
		$this->_values = $values;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 * @return Iterator<T>
	 */
	public function getIterator() : Iterator
	{
		return new ArrayIterator($this->_values);
	}
	
}
