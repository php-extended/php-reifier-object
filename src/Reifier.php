<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Iterator;
use PhpExtended\Ensurer\EnsurerInterface;
use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Parser\NotNullFilterIterator;
use ReflectionClass;
use Throwable;

/**
 * Reifier class file.
 * 
 * This class is a basic implementation of the ReifierInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Reifier implements ReifierInterface
{
	
	/**
	 * The configuration to be used.
	 * 
	 * @var ?ReifierConfigurationInterface
	 */
	protected ?ReifierConfigurationInterface $_configuration = null;
	
	/**
	 * The ensurer.
	 * 
	 * @var EnsurerInterface
	 */
	protected EnsurerInterface $_ensurer;
	
	/**
	 * The factories that this reifier has built to fill objects.
	 * 
	 * @template T of object
	 * @psalm-suppress UndefinedDocblockClass
	 * @var array<class-string<T>, ObjectFactoryLink<T>>
	 * @phpstan-ignore-next-line // unknown class PhpExtended\Reifier\T
	 */
	protected array $_factories = [];
	
	/**
	 * Builds a new Reifier with the given ensurer.
	 * 
	 * @param ?ReifierConfigurationInterface $configuration
	 * @param ?EnsurerInterface $ensurer
	 */
	public function __construct(?ReifierConfigurationInterface $configuration = null, ?EnsurerInterface $ensurer = null)
	{
		$this->_configuration = $configuration;
		if(null === $ensurer)
		{
			$ensurer = new LooseEnsurer();
		}
		$this->_ensurer = $ensurer;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::setConfiguration()
	 */
	public function setConfiguration(?ReifierConfigurationInterface $config) : ReifierInterface
	{
		$this->_configuration = $config;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::getConfiguration()
	 */
	public function getConfiguration() : ReifierConfigurationInterface
	{
		if(null === $this->_configuration)
		{
			$this->_configuration = new ReifierConfiguration();
		}
		
		return $this->_configuration;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reify()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reify(string $objectClass, array $data) : object
	{
		return $this->reifyAtDepths($objectClass, 0, '', $data, $this->getConfiguration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reifyNullable()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyNullable(string $objectClass, array $data) : ?object
	{
		return $this->reifyNullableAtDepths($objectClass, 0, '', $data, $this->getConfiguration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReify()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReify(string $objectClass, array $data, ?ReifierReportInterface $report = null, int $idx = 0) : ?object
	{
		return $this->tryReifyAtDepths($objectClass, 0, '', $data, $this->getConfiguration(), $report, $idx);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReifyNullable()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyNullable(string $objectClass, array $data, ?ReifierReportInterface $report = null, int $idx = 0) : ?object
	{
		return $this->tryReifyNullableAtDepths($objectClass, 0, '', $data, $this->getConfiguration(), $report, $idx);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reifyAll()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyAll(string $objectClass, array $datas) : array
	{
		return $this->reifyAllAtDepths($objectClass, 0, '', $datas, $this->getConfiguration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reifyAllNullable()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyAllNullable(string $objectClass, array $datas) : array
	{
		return $this->reifyAllNullableAtDepths($objectClass, 0, '', $datas, $this->getConfiguration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReifyAll()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyAll(string $objectClass, array $datas, ?ReifierReportInterface $report = null) : array
	{
		return $this->tryReifyAllAtDepths($objectClass, 0, '', $datas, $this->getConfiguration(), $report);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReifyAllNullable()
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyAllNullable(string $objectClass, array $datas, ?ReifierReportInterface $report = null) : array
	{
		return $this->tryReifyAllNullableAtDepths($objectClass, 0, '', $datas, $this->getConfiguration(), $report);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reifyIterator()
	 * @psalm-suppress InvalidReturnType
	 */
	public function reifyIterator(string $objectClass, Iterator $iterator) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ReifierIterator($this, $objectClass, $iterator, false, false, null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::reifyIteratorNullable()
	 * @psalm-suppress InvalidReturnType
	 */
	public function reifyIteratorNullable(string $objectClass, Iterator $iterator) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ReifierIterator($this, $objectClass, $iterator, true, false, null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReifyIterator()
	 * @psalm-suppress InvalidReturnType
	 */
	public function tryReifyIterator(string $objectClass, Iterator $iterator, ?ReifierReportInterface $report = null) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ReifierIterator($this, $objectClass, $iterator, false, true, $report));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierInterface::tryReifyIteratorNullable()
	 * @psalm-suppress InvalidReturnType
	 */
	public function tryReifyIteratorNullable(string $objectClass, Iterator $iterator, ?ReifierReportInterface $report = null) : Iterator
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return new NotNullFilterIterator(new ReifierIterator($this, $objectClass, $iterator, true, true, $report));
	}
	
	/**
	 * Gets the reification of a given object for the given subtree and given
	 * depths.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ReifierConfigurationInterface $config
	 * @return T
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function reifyAtDepths(string $objectClass, int $depths, string $path, array $data, ReifierConfigurationInterface $config) : object
	{
		$newObjectClass = $this->getPolymorphismClass($objectClass, $data, $config) ?? $objectClass;
		
		try
		{
			$concreteClass = $config->getImplementation($newObjectClass);
		}
		catch(MissingImplementationThrowable|MissingInnerTypeThrowable $e)
		{
			// refill the data, depths and expected class as they were missing
			throw new MissingImplementationException($data, $depths, $objectClass, $e->getFailedAttribute(), $path, null, -1, $e);
		}
		
		$factory = $this->getFactory($concreteClass);
		
		return $factory->applyTo(null, $data, $depths, $path, $config);
	}
	
	/**
	 * Gets the class choosen to be reified as for the given data.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data $data
	 * @param ReifierConfigurationInterface $config
	 * @return ?class-string<T>
	 */
	public function getPolymorphismClass(string $objectClass, array $data, ReifierConfigurationInterface $config) : ?string
	{
		foreach($config->getPolyMorphismsFor($objectClass) as /* $priority => */ $polymorphismList)
		{
			foreach($polymorphismList as $key => $polymorphismValue)
			{
				foreach($polymorphismValue as $value => $childClass)
				{
					if(isset($data[$key]) && $data[$key] === $value)
					{
						return $childClass;
					}
				}
			}
		}
		
		foreach($config->getPolymorphismsPresenceFor($objectClass) as /* $priority => */ $polymorphismList)
		{
			foreach($polymorphismList as $key => $childClass)
			{
				if(\array_key_exists($key, $data))
				{
					return $childClass;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the reification of a given nullable object for the given subtree
	 * and given depths.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ReifierConfigurationInterface $config
	 * @return ?T
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyNullableAtDepths(string $objectClass, int $depths, string $path, array $data, ReifierConfigurationInterface $config) : ?object
	{
		if([] === $data)
		{
			return null;
		}
		
		return $this->reifyAtDepths($objectClass, $depths, $path, $data, $config);
	}
	
	/**
	 * Tries to reify the given object for the given subtree at given depths.
	 * If the reification fails, null is returned. In this case, if the report
	 * is provided, it will be filled with the error.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ReifierConfigurationInterface $config
	 * @param ?ReifierReportInterface $report
	 * @param integer $idx
	 * @return ?T
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyAtDepths(string $objectClass, int $depths, string $path, array $data, ReifierConfigurationInterface $config, ?ReifierReportInterface $report = null, int $idx = 0) : ?object
	{
		try
		{
			return $this->reifyAtDepths($objectClass, $depths, $path, $data, $config);
		}
		catch(ReificationThrowable $exc)
		{
			if(null !== $report)
			{
				$messages = [];
				$path = $exc->getPath();
				$depths = $exc->getDepths();
				$eclass = $exc->getExpectedClass();
				/** @var Throwable $previous */
				$previous = $exc;
				
				do
				{
					$messages[] = $previous->getMessage();
					if($previous instanceof ReificationThrowable)
					{
						$path = $previous->getPath();
						$depths = $previous->getDepths();
						$eclass = $previous->getExpectedClass();
					}
					$previous = $previous->getPrevious();
				}
				while(null !== $previous);
				
				$report->addEntry(new ReifierReportEntry(
					$idx,
					$path,
					$depths,
					(string) \json_encode($exc->getData(), \JSON_PRETTY_PRINT),
					$eclass,
					(string) \implode("\n", $messages),
				));
			}
			
			return null;
		}
	}
	
	/**
	 * Tries to reify the given nullable object for the given subtree at given
	 * depths. If the reification fails, null is returned. In this case, if the
	 * report is provided, it will be filled with the error.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ReifierConfigurationInterface $config
	 * @param ?ReifierReportInterface $report
	 * @param integer $idx
	 * @return ?T
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyNullableAtDepths(string $objectClass, int $depths, string $path, array $data, ReifierConfigurationInterface $config, ?ReifierReportInterface $report = null, int $idx = 0) : ?object
	{
		try
		{
			return $this->reifyNullableAtDepths($objectClass, $depths, $path, $data, $config);
		}
		catch(ReificationThrowable $exc)
		{
			if(null !== $report)
			{
				$messages = [];
				$path = $exc->getPath();
				$depths = $exc->getDepths();
				$eclass = $exc->getExpectedClass();
				/** @var Throwable $previous */
				$previous = $exc;
				
				do
				{
					$messages[] = $previous->getMessage();
					if($previous instanceof ReificationThrowable)
					{
						$path = $previous->getPath();
						$depths = $previous->getDepths();
						$eclass = $previous->getExpectedClass();
					}
					$previous = $previous->getPrevious();
				}
				while(null !== $previous);
				
				$report->addEntry(new ReifierReportEntry(
					$idx,
					$path,
					$depths,
					(string) \json_encode($exc->getData(), \JSON_PRETTY_PRINT),
					$eclass,
					(string) \implode("\n", $messages),
				));
			}
			
			return null;
		}
	}
	
	/**
	 * Tries to reify all of the given objects for the given subtree at given
	 * depths. If the reification of any of the objects in the first level array
	 * fails, then a ReificationException is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string,  array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ReifierConfigurationInterface $config
	 * @return array<integer, T>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyAllAtDepths(string $objectClass, int $depths, string $path, array $datas, ReifierConfigurationInterface $config) : array
	{
		$reified = [];
		
		foreach($datas as $data)
		{
			$reified[] = $this->reifyAtDepths($objectClass, $depths, $path, $data, $config);
		}
		
		return $reified;
	}
	
	/**
	 * Tries to reify all of the given nullable objects for the given subtree
	 * at given depths. If the reification of any of the objects in the first
	 * level array fails, then a ReificationException is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string,  array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ReifierConfigurationInterface $config
	 * @return array<integer, T>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function reifyAllNullableAtDepths(string $objectClass, int $depths, string $path, array $datas, ReifierConfigurationInterface $config) : array
	{
		$reified = [];
		
		foreach($datas as $data)
		{
			$nreified = $this->reifyNullableAtDepths($objectClass, $depths, $path, $data, $config);
			if(null !== $nreified)
			{
				$reified[] = $nreified;
			}
		}
		
		return $reified;
	}
	
	/**
	 * Tries to reify all of the given objects for the given subtree at given
	 * depths. If the reification of any of the objects in the first
	 * level array fails, then the object is not included into the result array.
	 * 
	 * If the report is given, then all unreifiable data chunks are notified
	 * into it.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string,  array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ReifierConfigurationInterface $config
	 * @param ?ReifierReportInterface $report
	 * @return array<integer, T>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyAllAtDepths(string $objectClass, int $depths, string $path, array $datas, ReifierConfigurationInterface $config, ?ReifierReportInterface $report = null) : array
	{
		$reified = [];
		$idx = 0;
		
		foreach($datas as $data)
		{
			$treified = $this->tryReifyAtDepths($objectClass, $depths, $path, $data, $config, $report, $idx);
			if(null !== $treified)
			{
				$reified[] = $treified;
			}
			$idx++;
		}
		
		return $reified;
	}
	
	/**
	 * Tries to reify all of the given objects for the given subtree at given
	 * depths. If the reification of any of the objects in the first
	 * level array fails, or if the object is nullable, then the object is not
	 * included into the result array.
	 *
	 * If the report is given, then all unreifiable data chunks are notified
	 * into it.
	 *
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param integer $depths
	 * @param string $path
	 * @param array<integer|string,  array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ReifierConfigurationInterface $config
	 * @param ?ReifierReportInterface $report
	 * @return array<integer, T>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 */
	public function tryReifyAllNullableAtDepths(string $objectClass, int $depths, string $path, array $datas, ReifierConfigurationInterface $config, ?ReifierReportInterface $report = null) : array
	{
		$reified = [];
		$idx = 0;
		
		foreach($datas as $data)
		{
			$treified = $this->tryReifyNullableAtDepths($objectClass, $depths, $path, $data, $config, $report, $idx);
			if(null !== $treified)
			{
				$reified[] = $treified;
			}
			$idx++;
		}
		
		return $reified;
	}
	
	/**
	 * Gets the factory to build objects of the expected class.
	 * 
	 * @template T of object
	 * @param class-string<T> $concreteClass
	 * @return ObjectFactoryLink<T>
	 * @psalm-suppress InvalidReturnType
	 */
	public function getFactory(string $concreteClass) : ObjectFactoryLink
	{
		if(isset($this->_factories[$concreteClass]))
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
			return $this->_factories[$concreteClass];
		}
		
		$rclass = new ReflectionClass($concreteClass);
		$fact4 = new ObjectFactoryIgnore($this, $this->_ensurer, $rclass);
		$fact3 = new ObjectFactoryField($this, $this->_ensurer, $rclass, $fact4);
		$fact2 = new ObjectFactoryMethod($this, $this->_ensurer, $rclass, $fact3);
		$fact1 = new ObjectFactoryCtor($this, $this->_ensurer, $rclass, $fact2);
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
		return $this->_factories[$concreteClass] = $fact1;
	}
	
}
