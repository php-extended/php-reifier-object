<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use LogicException;
use PhpExtended\DateTime\AbstractDateTimeParser;
use PhpExtended\Ensurer\EnsurerInterface;
use PhpExtended\Parser\ParseThrowable;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use Stringable;

/**
 * ObjectFactoryLink class file.
 * 
 * This class is a link into a transformative chain that transforms one slice
 * of the data subtree array into object form.
 * 
 * @author Anastaszor
 * @template T of object
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ObjectFactoryLink implements Stringable
{
	
	/**
	 * The reifier.
	 *
	 * @var Reifier
	 */
	protected Reifier $_reifier;
	
	/**
	 * The ensurer.
	 *
	 * @var EnsurerInterface
	 */
	protected EnsurerInterface $_ensurer;
	
	/**
	 * The reflection class.
	 *
	 * @var ReflectionClass<T>
	 */
	protected ReflectionClass $_rclass;
	
	/**
	 * The next object factory link.
	 * 
	 * @var ?ObjectFactoryLink<T>
	 */
	protected ?ObjectFactoryLink $_nextLink = null;
	
	/**
	 * The fields and the type they should resolve to.
	 * [method_name => [param_name => type]].
	 *
	 * @var array<string, array<string, ReflectionType>>
	 */
	protected array $_setterTypes = [];
	
	/**
	 * Builds a new ObjectFactoryLink for objects of the given concrete class.
	 * 
	 * @param Reifier $reifier
	 * @param EnsurerInterface $ensurer
	 * @param ReflectionClass<T> $rclass
	 * @param ObjectFactoryLink<T> $next
	 */
	public function __construct(Reifier $reifier, EnsurerInterface $ensurer, ReflectionClass $rclass, ?ObjectFactoryLink $next = null)
	{
		$this->_reifier = $reifier;
		$this->_ensurer = $ensurer;
		$this->_rclass = $rclass;
		$this->_nextLink = $next;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'['.$this->_rclass->getName().']@'.\spl_object_hash($this);
	}
	
	/**
	 * Effectively does the transform from array data structure to object
	 * data structure.
	 * 
	 * This method is expected to be overridden by specific factory links.
	 * 
	 * @param ?T $object the base object to work with
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param integer $depths
	 * @param string $path
	 * @param ReifierConfigurationInterface $config
	 * @return T
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function applyTo($object, array $data, int $depths, string $path, ReifierConfigurationInterface $config) : object
	{
		if(null !== $this->_nextLink)
		{
			return $this->_nextLink->applyTo($object, $data, $depths + 1, $path, $config);
		}
		
		if(null !== $object)
		{
			return $object;
		}
		
		$message = 'NULL object provided and no next link available to build it for objects of class {class}';
		$context = ['{class}' => $this->_rclass->getName()];
		
		throw new ReificationException($data, $depths, $this->_rclass->getName(), '__construct', $path, \strtr($message, $context));
	}
	
	/**
	 * Gets a camelCase version of the given name.
	 *
	 * @param string $name
	 * @return string
	 */
	public function getCamelCaseName(string $name) : string
	{
		return \lcfirst($this->getPascalCaseName($name));
	}
	
	/**
	 * Gets a PascalCase version of the given name.
	 *
	 * @param string $name
	 * @return string
	 */
	public function getPascalCaseName(string $name) : string
	{
		return \ucfirst(\trim((string) \preg_replace_callback('#_(\\w?)#', function(array $matches) : string
		{
			/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
			return \strtoupper((string) $matches[1]);
		}, \str_replace(['.', '-'], '_', $this->getSnakeCaseName($name)))));
	}
	
	/**
	 * Gets a snake_case version of the given name.
	 *
	 * @param string $name
	 * @return string
	 */
	public function getSnakeCaseName(string $name) : string
	{
		return \lcfirst(\trim((string) \preg_replace_callback('#(^|[a-z0-1])([A-Z]+)#', function(array $matches) : string
		{
			/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
			return ((string) $matches[1]).'_'.((string) \mb_strtolower((string) $matches[2]));
		}, $this->stripNonFieldChars($name)), '_'));
	}
	
	/**
	 * Gets a dash-case version of the given name.
	 *
	 * @param string $name
	 * @return string
	 */
	public function getDashCaseName(string $name) : string
	{
		return \str_replace('_', '-', $this->getSnakeCaseName($name));
	}
	
	/**
	 * Forces the value to take the form wanted by the reflection type.
	 *
	 * @param string $fieldName
	 * @param ReflectionType $type
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $depths
	 * @param string $path
	 * @param ReifierConfigurationInterface $config
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 */
	public function coerceValue(string $fieldName, ReflectionType $type, $value, int $depths, string $path, ReifierConfigurationInterface $config)
	{
		// $type->__toString() is deprecated since php7.1
		$typeName = ($type instanceof ReflectionNamedType) ? $type->getName() : 'Unnamed type';
		
		return $this->coerceValueType($fieldName, $typeName, $type->allowsNull(), $value, $depths, $path, $config);
	}
	
	/**
	 * @param string $fieldName
	 * @param string $typeName
	 * @param bool $nullAllowed
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $depths
	 * @param string $path
	 * @param ReifierConfigurationInterface $config
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function coerceValueType(string $fieldName, string $typeName, bool $nullAllowed, $value, int $depths, string $path, ReifierConfigurationInterface $config)
	{
		try
		{
			switch($typeName)
			{
				case 'bool':
					return $nullAllowed
						? $this->_ensurer->asBooleanOrNull($value)
						: $this->_ensurer->asBoolean($value);
					
				case 'int':
					return $nullAllowed
						? $this->_ensurer->asIntegerOrNull($value)
						: $this->_ensurer->asInteger($value);
					
				case 'float':
					return $nullAllowed
						? $this->_ensurer->asFloatOrNull($value)
						: $this->_ensurer->asFloat($value);
					
				case 'string':
					return $nullAllowed
						? $this->_ensurer->asStringOrNull($value)
						: $this->_ensurer->asString($value);
					
				case 'array':
					if($nullAllowed && null === $value)
					{
						return null;
					}
					
					return $this->coerceToArray($fieldName, $value, $depths, $path, $config);
			}
			
			// special case for iterables and \Iterator collection
			if(\is_array($value) && ('iterable' === $typeName || Iterator::class === $typeName || \is_subclass_of($typeName, Iterator::class)))
			{
				try
				{
					$innerObjectClass = $config->getIterableInnerType($this->_rclass->getName(), $fieldName);
				}
				catch(MissingInnerTypeThrowable $exc)
				{
					$message = 'Failed to get inner class for iterable of class "{class}" as argument named "{argname}" of parent class {parent}';
					$context = ['{class}' => $typeName, '{argname}' => $fieldName, '{parent}' => $this->_rclass->getName()];
					/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
					throw new MissingInnerTypeException($value, $depths, $typeName, $fieldName, $path, \strtr($message, $context), -1, $exc);
				}
				
				$values = [];
				
				foreach($value as $key => $inner)
				{
					try
					{
						$newFieldName = $fieldName.'['.((string) $key).']';
						$newPath = $path.'['.((string) $key).']';
						$values[$key] = $this->coerceValueType($newFieldName, $innerObjectClass, false, $inner, $depths + 1, $newPath, $config);
					}
					catch(ReificationException $exc)
					{
						if(!$config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName.'[]'))
						{
							throw $exc;
						}
						
						// else just ignore it
					}
				}
				
				return new ArrayIterator($values);
			}
			
			// generic object
			try
			{
				/** @var class-string<T> $typeName */
				return $this->coerceToObject($typeName, $fieldName, $nullAllowed, $value, $depths, $path, $config);
			}
			catch(ReificationException $exc)
			{
				if(!$nullAllowed || !$config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName))
				{
					throw $exc;
				}
				
				return null;
			}
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to coerce value {tval} to {type}';
			$context = ['{tval}' => \gettype($value), '{type}' => $typeName];
			
			throw new ReificationException($value, $depths, $this->_rclass->getName(), $fieldName, $path, \strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Forces the value to be an array with inner objects of the right type,
	 * such type is given by the configuration.
	 *
	 * @param string $fieldName
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $depths
	 * @param string $path
	 * @param ReifierConfigurationInterface $config
	 * @return array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @psalm-suppress InvalidReturnType
	 */
	public function coerceToArray(string $fieldName, $value, int $depths, string $path, ReifierConfigurationInterface $config) : array
	{
		if(null === $value)
		{
			return [];
		}
		
		try
		{
			$innerType = $config->getIterableInnerType($this->_rclass->getName(), $fieldName);
		}
		catch(MissingInnerTypeThrowable $exc)
		{
			// rethrow with the full information
			throw new MissingInnerTypeException($value, $depths, $this->_rclass->getName(), $fieldName, $path, null, -1, $exc);
		}
		
		// TODO if not found, try to parse docblock
		
		try
		{
			switch($innerType)
			{
				case 'bool':
					return $this->_ensurer->asArrayOfBooleans($value);
					
				case 'int':
					return $this->_ensurer->asArrayOfIntegers($value);
					
				case 'float':
					return $this->_ensurer->asArrayOfFloats($value);
					
				case 'string':
					return $this->_ensurer->asArrayOfStrings($value);
					
				case 'array':
					$values = [];
					
					/** @psalm-suppress PossiblyInvalidArgument */
					foreach($this->_ensurer->asArray($value) as $key => $inner)
					{
						$key = (string) $key;
						
						try
						{
							$values[$key] = $this->coerceToArray($fieldName.'[]', $inner, $depths + 1, $path.'['.$key.']', $config);
						}
						catch(ReificationException $exc)
						{
							if(!$config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName.'[]'))
							{
								throw $exc;
							}
							
							// else just ignore
						}
					}
					
					/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
					return $values;
			}
			
			$values = [];
			
			// generic object
			/** @psalm-suppress PossiblyInvalidArgument */
			foreach($this->_ensurer->asArray($value) as $key => $inner)
			{
				$key = (string) $key;
				
				try
				{
					/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
					$value = $this->coerceToObject($innerType, $fieldName, true, $inner, $depths + 1, $path.'['.$key.']', $config);
					if(null !== $value)
					{
						$values[$key] = $value;
					}
				}
				catch(ReificationException $exc)
				{
					if(!$config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName))
					{
						throw $exc;
					}
					
					// else just ignore
				}
			}
			
			return $values;
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to coerce value {tval} to array of {type}';
			$context = ['{tval}' => \gettype($value), '{type}' => $innerType];
			
			throw new ReificationException($value, $depths, $this->_rclass->getName(), $fieldName, $path, \strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Transforms the given value into object. If it is a scalar, it will be
	 * transformed into a string and parsed over by the designated parser. If
	 * it is an object it will be cast, and if it is an array it will be
	 * built from the reifier, going one level deeper.
	 *
	 * @template V of object
	 * @param class-string<V> $className
	 * @param string $fieldName
	 * @param boolean $nullAllowed
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @param integer $depths
	 * @param string $path
	 * @param ReifierConfigurationInterface $config
	 * @return ?V
	 * @throws MissingImplementationThrowable
	 * @throws MissingInnerTypeThrowable
	 * @throws MissingParserThrowable
	 * @throws ReificationException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function coerceToObject(string $className, string $fieldName, bool $nullAllowed, $value, int $depths, string $path, ReifierConfigurationInterface $config) : ?object
	{
		if($nullAllowed && null === $value)
		{
			return null;
		}
		
		if(\is_array($value))
		{
			return $this->_reifier->reifyAtDepths($className, $depths + 1, $path, $value, $config);
		}
		
		if(\is_object($value))
		{
			if($value instanceof $className)
			{
				return $value;
			}
			
			if($nullAllowed && $config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName))
			{
				return null;
			}
			
			$message = 'Failed to cast object of class {class} into expected class {interface}';
			$context = ['{class}' => \get_class($value), '{interface}' => $className];
			
			throw new ReificationException($value, $depths, $this->_rclass->getName(), $fieldName, $path, \strtr($message, $context));
		}
		
		if(\is_scalar($value))
		{
			if($nullAllowed && \is_string($value) && \trim($value) === '' && $config->isFieldEmptyAsNull($this->_rclass->getName(), $fieldName))
			{
				return null;
			}
			
			try
			{
				$parser = $config->getParser($className);
			}
			catch(MissingParserThrowable $e)
			{
				$message = 'Failed to parse value "{val}" of type {type} into {class} : no parser available.';
				$context = ['{val}' => $value, '{type}' => \gettype($value), '{class}' => $className];
				
				throw new MissingParserException($value, $depths, $this->_rclass->getName(), $fieldName, $path, \strtr($message, $context), -1, $e);
			}
			
			// saves the current formats of the parser, get the explicit ones
			$foundFormats = [];
			$savedFormats = [];
			if($parser instanceof AbstractDateTimeParser)
			{
				$foundFormats = $config->getDateTimeFormats($this->_rclass->getName(), $fieldName);
				if([] !== $foundFormats)
				{
					$savedFormats = $parser->getFormats();
					$parser->setFormats($foundFormats);
				}
			}
			
			try
			{
				$object = $parser->parse((string) $value);
			}
			catch(ParseThrowable $e)
			{
				if($nullAllowed && $config->isFieldAllowedToFail($this->_rclass->getName(), $fieldName))
				{
					return null;
				}
				
				$message = 'Failed to parse value "{val}" of type {type} into {class}';
				$context = ['{val}' => $value, '{type}' => \gettype($value), '{class}' => $className];
				
				throw new ReificationException($value, $depths, $this->_rclass->getName(), $fieldName, $path, \strtr($message, $context), -1, $e);
			}
			
			// then put them back
			if($parser instanceof AbstractDateTimeParser && [] !== $foundFormats)
			{
				$parser->setFormats($savedFormats);
			}
			
			return $object;
		}
		
		throw new ReificationException($value, $depths, $this->_rclass->getName(), $fieldName, $path);
	}
	
	/**
	 * Gets the type of that is expected from the given param of method.
	 *
	 * @param ReflectionMethod $rMethod
	 * @param ReflectionParameter $rParam
	 * @return ReflectionType
	 * @throws LogicException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function resolveTypeFromParam(ReflectionMethod $rMethod, ReflectionParameter $rParam) : ReflectionType
	{
		if(isset($this->_setterTypes[$rMethod->getName()][$rParam->getName()]))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()];
		}
		
		$rType = $rParam->getType();
		if(null !== $rType)
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = $rType;
		}
		
		$getterPossibleNames = [];
		
		foreach(['get', 'is', 'has'] as $prefix)
		{
			$getterPossibleNames[] = $prefix.\ucfirst($rParam->getName()); // camel case
			$getterPossibleNames[] = $prefix.'_'.$rParam->getName();       // snake_case
			$getterPossibleNames[] = $prefix.\ucfirst((string) \preg_replace_callback('#_+([A-Za-z0-9])#', function(array $matches) : string
			{
				/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
				return (string) \mb_strtoupper((string) $matches[1]); // forced camel case
			}, $rParam->getName()));
		}
		
		// https://stackoverflow.com/questions/6250226/php-get-variable-type-hint-using-reflection
		foreach($this->_rclass->getMethods(ReflectionMethod::IS_PUBLIC) as $rcMethod)
		{
			/** @var ReflectionMethod $rcMethod */
			if(0 === $rcMethod->getNumberOfParameters()
				&& \in_array($rcMethod->getName(), $getterPossibleNames, true)
			) {
				$returnType = $rcMethod->getReturnType();
				if(null !== $returnType)
				{
					return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = $returnType;
				}
			}
		}
		
		// TODO preg_match the export https://stackoverflow.com/questions/4513867/php-reflection-get-method-parameter-type-as-string
		// TODO if native type hint is not available, use docblocks
		
		// last resort, use name of the type
		if(\in_array($rParam->getName(), ['bool', 'boolean'], true))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = new CustomReflectionType('bool', false);
		}
		
		if(\in_array($rParam->getName(), ['int', 'integer'], true))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = new CustomReflectionType('int', false);
		}
		
		if(\in_array($rParam->getName(), ['float', 'double', 'flt', 'dbl', 'real'], true))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = new CustomReflectionType('float', false);
		}
		
		if(\in_array($rParam->getName(), ['string', 'str', 'needle'], true))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = new CustomReflectionType('string', false);
		}
		
		if(\in_array($rParam->getName(), ['array', 'list', 'map', 'haystack'], true))
		{
			return $this->_setterTypes[$rMethod->getName()][$rParam->getName()] = new CustomReflectionType('array', false);
		}
		
		$message = 'Failed to guess return type for parameter {class}::{method}({param})';
		$context = ['{class}' => $this->_rclass->getNamespaceName(), '{method}' => $rMethod->getName(), '{param}' => $rParam->getName()];
		
		throw new LogicException(\strtr($message, $context));
	}
	
	/**
	 * Strips non alphanumerial characters off the name value.
	 * 
	 * @param string $name
	 * @return string
	 */
	protected function stripNonFieldChars(string $name) : string
	{
		return (string) \preg_replace('#[^A-Za-z0-9_.-]+#', '_', $name);
	}
	
}
