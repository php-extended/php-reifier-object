<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

/**
 * ReifierReportEntry class file.
 * 
 * This is a simple implementation of the ReifierReportEntryInterface.
 * 
 * @author Anastaszor
 */
class ReifierReportEntry implements ReifierReportEntryInterface
{
	
	/**
	 * The index from the root iteration when this failed.
	 * 
	 * @var integer
	 */
	protected int $_index;
	
	/**
	 * The path from the root object when this failed.
	 * 
	 * @var string
	 */
	protected string $_path;
	
	/**
	 * The offset at which reification failed.
	 * 
	 * @var integer
	 */
	protected int $_offset;
	
	/**
	 * A string representation of the data that failed reification.
	 * 
	 * @var ?string
	 */
	protected ?string $_data;
	
	/**
	 * The class of the object that failed reification.
	 * 
	 * @var class-string
	 */
	protected string $_classname;
	
	/**
	 * The message of the error.
	 * 
	 * @var string
	 */
	protected string $_message;
	
	/**
	 * Builds a new ReifierReportEntry with its data.
	 * 
	 * @param integer $index
	 * @param string $path
	 * @param integer $offset
	 * @param string $data
	 * @param class-string $classname
	 * @param string $message
	 */
	public function __construct(int $index, string $path, int $offset, ?string $data, string $classname, string $message)
	{
		$this->_index = $index;
		$this->_path = $path;
		$this->_offset = $offset;
		$this->_data = $data;
		$this->_classname = $classname;
		$this->_message = $message;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getIndex()
	 */
	public function getIndex() : int
	{
		return $this->_index;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getPath()
	 */
	public function getPath() : string
	{
		return $this->_path;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getOffset()
	 */
	public function getOffset() : int
	{
		return $this->_offset;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getData()
	 */
	public function getData() : ?string
	{
		return $this->_data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getClassname()
	 */
	public function getClassname() : string
	{
		return $this->_classname;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierReportEntryInterface::getMessage()
	 */
	public function getMessage() : string
	{
		return $this->_message;
	}
	
}
