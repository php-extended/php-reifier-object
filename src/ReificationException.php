<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use RuntimeException;
use Throwable;

/**
 * ReificationException class file.
 * 
 * Reification Exceptions are thrown when a subtree of data cannot be converted
 * to object data structures for any reason.
 * 
 * @author Anastaszor
 */
class ReificationException extends RuntimeException implements ReificationThrowable
{
	
	/**
	 * The data of the subtree that failed to comply.
	 * 
	 * @var null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	protected $_data;
	
	/**
	 * The depths of the subtree that failed to comply.
	 * 
	 * @var integer
	 */
	protected int $_depths;
	
	/**
	 * The full path from root object to the faulty attribute.
	 * 
	 * @var string
	 */
	protected string $_path;
	
	/**
	 * The name of the class that failed to comply.
	 * 
	 * @var class-string
	 */
	protected string $_expectedClass;
	
	/**
	 * The attribute of the expected class that failed to comply.
	 * 
	 * @var string
	 */
	protected string $_failedAttribute;
	
	/**
	 * Builds a new ReificationException with the given data attributes and
	 * previous status.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param integer $depths
	 * @param class-string $expectedClass
	 * @param string $failedAttribute
	 * @param string $path
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct($data, int $depths, string $expectedClass, string $failedAttribute, string $path, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_data = $data;
		$this->_depths = $depths;
		$this->_path = $path;
		$this->_expectedClass = $expectedClass;
		$this->_failedAttribute = $failedAttribute;
		
		$newmsg = 'Failed to reify {data} into {class} at depths {d}, attribute {attr} at {path}';
		$expand = [];
		$ndata = !\is_array($this->_data) ? ['data' => $this->_data] : (array) $this->_data;
		
		foreach($ndata as $key => $value)
		{
			$expand[] = '"'.((string) $key).'" => '.(\is_object($value) ? \get_class($value) : \gettype($value));
		}
		
		$context = [
			'{data}' => '['.\implode(', ', $expand).']',
			'{class}' => $this->_expectedClass,
			'{d}' => $this->_depths,
			'{attr}' => $this->_failedAttribute,
			'{path}' => $path,
		];
		$newmsg = \strtr($newmsg, $context);
		
		if(null !== $message)
		{
			$newmsg .= ' : '.$message;
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($newmsg, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReificationThrowable::getDepths()
	 */
	public function getDepths() : int
	{
		return $this->_depths;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReificationThrowable::getPath()
	 */
	public function getPath() : string
	{
		return $this->_path;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReificationThrowable::getData()
	 */
	public function getData()
	{
		return $this->_data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReificationThrowable::getExpectedClass()
	 */
	public function getExpectedClass() : string
	{
		return $this->_expectedClass;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReificationThrowable::getFailedAttribute()
	 */
	public function getFailedAttribute() : string
	{
		return $this->_failedAttribute;
	}
	
}
