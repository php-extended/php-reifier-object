<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use LogicException;
use PhpExtended\Ensurer\EnsurerInterface;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use Throwable;

/**
 * ObjectFactoryMethod class file.
 * 
 * This class is a link that transforms an object that is already built by
 * adding information via the public fields it holds.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends ObjectFactoryLink<T>
 */
class ObjectFactoryMethod extends ObjectFactoryLink
{
	
	/**
	 * The keys that are ignored by this factory.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_ignoredKeys = [];
	
	/**
	 * The getters available in the class.
	 *
	 * @var array<string, ReflectionMethod>
	 */
	protected array $_setters = [];
	
	/**
	 * Builds a new ObjectFactoryMethod for the given class.
	 * 
	 * @param Reifier $reifier
	 * @param EnsurerInterface $ensurer
	 * @param ReflectionClass<T> $rclass
	 * @param ObjectFactoryLink<T> $next
	 */
	public function __construct(Reifier $reifier, EnsurerInterface $ensurer, ReflectionClass $rclass, ?ObjectFactoryLink $next = null)
	{
		parent::__construct($reifier, $ensurer, $rclass, $next);
		
		foreach($this->_rclass->getMethods(ReflectionMethod::IS_PUBLIC) as $rMethod)
		{
			/** @var ReflectionMethod $rMethod */
			if($rMethod->isStatic())
			{
				continue;
			}
			
			// not a setter
			if(0 !== \mb_strpos($rMethod->getName(), 'set'))
			{
				continue;
			}
			
			// a setter has at least one parameter and at most one required parameter
			if(!(0 < $rMethod->getNumberOfParameters() && 1 >= $rMethod->getNumberOfRequiredParameters()))
			{
				continue;
			}
			
			// strip the "set" part and get a slug of the parameter name
			$key = \mb_substr((string) $rMethod->getName(), 3);
			$this->_setters[\lcfirst($key)] = $rMethod; // right camel case
			$this->_setters[\mb_strtolower($key)] = $rMethod; // lowercase
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ObjectFactoryLink::applyTo()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function applyTo($object, array $data, int $depths, string $path, ReifierConfigurationInterface $config) : object
	{
		if(null === $object)
		{
			return parent::applyTo($object, $data, $depths, $path, $config);
		}
		
		$remainingData = [];
		
		foreach($data as $key => $value)
		{
			$key = (string) $key;
			$lkey = \mb_strtolower($key);
			
			if(isset($this->_ignoredKeys[$key]))
			{
				$remainingData[$key] = $value;
				
				continue;
			}
			
			$setter = $this->_setters[$key] ?? null;
			
			if(null === $setter)
			{
				$aliasFieldName = $config->getFieldNameFromAlias($this->_rclass->getName(), $key);
				if($key !== $aliasFieldName)
				{
					$setter = $this->_setters[$aliasFieldName] ?? null;
				}
				$lowerAliasFieldName = \mb_strtolower($aliasFieldName);
				if($lkey !== $lowerAliasFieldName)
				{
					$setter = $this->_setters[$lowerAliasFieldName] ?? null;
				}
			}
			
			if(null === $setter)
			{
				$setter = $this->lookUpCamelCaseSetter($key, $config);
			}
			
			if(null === $setter)
			{
				$setter = $this->lookUpDashCaseSetter($key, $config);
			}
			
			if(null === $setter)
			{
				$setter = $this->lookUpPascalCaseSetter($key, $config);
			}
			
			if(null === $setter)
			{
				$setter = $this->lookUpSnakeCaseSetter($key, $config);
			}
			
			if(null === $setter)
			{
				$remainingData[$key] = $value;
				$this->_ignoredKeys[$key] = 1;
				
				continue;
			}
			
			$setterArgs = $setter->getParameters();
			// unnecessary check because of filtering in __construct
			// but it makes psalm happy
			if(!isset($setterArgs[0]))
			{
				// @codeCoverageIgnoreStart
				continue;
				// @codeCoverageIgnoreEnd
			}
			
			/** @var ReflectionParameter $setterArg */
			$setterArg = $setterArgs[0];
			
			try
			{
				$rType = $this->resolveTypeFromParam($setter, $setterArg);
			}
			catch(LogicException $exc)
			{
				throw new ReificationException($data, $depths, $this->_rclass->getName(), $key, $path.'.'.$key, null, -1, $exc);
			}
			
			try
			{
				$value = $this->coerceValue($key, $rType, $value, $depths, $path.'.'.$key, $config);
			}
			catch(ReificationException $exc)
			{
				throw new ReificationException($data, $exc->getDepths(), $this->_rclass->getName(), $key, $path.'.'.$key, null, -1, $exc);
			}
			
			try
			{
				$setter->invoke($object, $value);
			}
			catch(Throwable $exc)
			{
				throw new ReificationException($data, $depths, $this->_rclass->getName(), $key, $path.'.'.$key, null, -1, $exc);
			}
		}
		
		return parent::applyTo($object, $remainingData, $depths, $path, $config);
	}
	
	/**
	 * Looks up for the given setter method from the given field name for the
	 * given key.
	 * 
	 * @param string $key
	 * @param string $fieldName
	 * @param ReifierConfigurationInterface $config
	 * @return ?ReflectionMethod
	 */
	public function lookUpSetter(string $key, string $fieldName, ReifierConfigurationInterface $config) : ?ReflectionMethod
	{
		if(isset($this->_setters[$fieldName]))
		{
			$config->addFieldNameAlias($this->_rclass->getName(), $key, $fieldName);
			
			return $this->_setters[$fieldName];
		}
		
		$lowerFieldName = \mb_strtolower($fieldName);
		
		if(isset($this->_setters[$lowerFieldName]))
		{
			$config->addFieldNameAlias($this->_rclass->getName(), $key, $lowerFieldName);
			
			return $this->_setters[$lowerFieldName];
		}
		
		return null;
	}
	
	/**
	 * Looks up for the given setter method from the name of the given key,
	 * applying the camel case setter policy if available.
	 * 
	 * @param string $key
	 * @param ReifierConfigurationInterface $config
	 * @return ?ReflectionMethod
	 */
	public function lookUpCamelCaseSetter(string $key, ReifierConfigurationInterface $config) : ?ReflectionMethod
	{
		return $this->lookUpSetter($key, $this->getCamelCaseName($key), $config);
	}
	
	/**
	 * Looks up for the given setter method from the name of the given key,
	 * applying the dash case setter policy if available.
	 * 
	 * @param string $key
	 * @param ReifierConfigurationInterface $config
	 * @return ?ReflectionMethod
	 */
	public function lookUpDashCaseSetter(string $key, ReifierConfigurationInterface $config) : ?ReflectionMethod
	{
		return $this->lookUpSetter($key, $this->getDashCaseName($key), $config);
	}
	
	/**
	 * Looks up for the given setter method from the name of the given key,
	 * applying the pascal case setter policy if available.
	 * 
	 * @param string $key
	 * @param ReifierConfigurationInterface $config
	 * @return ?ReflectionMethod
	 */
	public function lookUpPascalCaseSetter(string $key, ReifierConfigurationInterface $config) : ?ReflectionMethod
	{
		return $this->lookUpSetter($key, $this->getPascalCaseName($key), $config);
	}
	
	/**
	 * Looks up for the given setter method from the name of the given key,
	 * applying the snake case policy if available.
	 * 
	 * @param string $key
	 * @param ReifierConfigurationInterface $config
	 * @return ?ReflectionMethod
	 */
	public function lookUpSnakeCaseSetter(string $key, ReifierConfigurationInterface $config) : ?ReflectionMethod
	{
		return $this->lookUpSetter($key, $this->getSnakeCaseName($key), $config);
	}
	
}
