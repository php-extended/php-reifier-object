<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use ReflectionNamedType;

/**
 * CustomReflectionType class file.
 * 
 * This class is an instanciable reflection type with the right parameters.
 * 
 * @author Anastaszor
 * @psalm-immutable
 */
class CustomReflectionType extends ReflectionNamedType
{
	
	/**
	 * The name of the type.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Whether this type allows null.
	 * 
	 * @var boolean
	 */
	protected bool $_allowsNull;
	
	/**
	 * Builds a new ReflectionType with a constructor.
	 * 
	 * @param string $name
	 * @param boolean $allowsNull
	 */
	public function __construct(string $name, bool $allowsNull)
	{
		$this->_name = $name;
		$this->_allowsNull = $allowsNull;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ReflectionNamedType::__toString()
	 */
	public function __toString() : string
	{
		return ($this->allowsNull() ? '?' : '').$this->getName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ReflectionNamedType::getName()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getName() : string
	{
		/** @psalm-suppress LessSpecificReturnStatement */
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ReflectionNamedType::allowsNull()
	 */
	public function allowsNull() : bool
	{
		return $this->_allowsNull;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ReflectionNamedType::isBuiltin()
	 * @psalm-immutable
	 */
	public function isBuiltin() : bool
	{
		return \in_array($this->_name, [
			'bool', 'int', 'float', 'string', 'array', 'callable', 'iterable', 'object', 'resource',
		], true);
	}
	
}
