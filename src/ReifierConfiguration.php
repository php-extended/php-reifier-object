<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use IteratorAggregate;
use PhpExtended\DateTime\DateTimeImmutableParser;
use PhpExtended\DateTime\DateTimeParser;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailAddressListInterface;
use PhpExtended\Email\EmailAddressListParser;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxGroupInterface;
use PhpExtended\Email\MailboxGroupList;
use PhpExtended\Email\MailboxGroupListInterface;
use PhpExtended\Email\MailboxGroupListParser;
use PhpExtended\Email\MailboxGroupParser;
use PhpExtended\Email\MailboxInterface;
use PhpExtended\Email\MailboxList;
use PhpExtended\Email\MailboxListInterface;
use PhpExtended\Email\MailboxListParser;
use PhpExtended\Email\MailboxParser;
use PhpExtended\HttpClient\AcceptLanguageChain;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv4AddressParser;
use PhpExtended\Ip\Ipv4Network;
use PhpExtended\Ip\Ipv4NetworkInterface;
use PhpExtended\Ip\Ipv4NetworkParser;
use PhpExtended\Ip\Ipv6Address;
use PhpExtended\Ip\Ipv6AddressInterface;
use PhpExtended\Ip\Ipv6AddressParser;
use PhpExtended\Ip\Ipv6Network;
use PhpExtended\Ip\Ipv6NetworkInterface;
use PhpExtended\Ip\Ipv6NetworkParser;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameInterface;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeMultiInterface;
use PhpExtended\Ldap\LdapFilterParser;
use PhpExtended\Mac\MacAddress48Bits;
use PhpExtended\Mac\MacAddress48BitsInterface;
use PhpExtended\Mac\MacAddress48Parser;
use PhpExtended\Mac\MacAddress64Bits;
use PhpExtended\Mac\MacAddress64BitsInterface;
use PhpExtended\Mac\MacAddress64Parser;
use PhpExtended\MimeType\MimeType;
use PhpExtended\MimeType\MimeTypeInterface;
use PhpExtended\MimeType\MimeTypeParser;
use PhpExtended\Parser\ParserInterface;
use PhpExtended\Ulid\Ulid;
use PhpExtended\Ulid\UlidInterface;
use PhpExtended\Ulid\UlidParser;
use PhpExtended\Uri\UriParser;
use PhpExtended\Uuid\Uuid;
use PhpExtended\Uuid\UuidInterface;
use PhpExtended\Uuid\UuidParser;
use PhpExtended\Version\Version;
use PhpExtended\Version\VersionConstraintInterface;
use PhpExtended\Version\VersionConstraintParser;
use PhpExtended\Version\VersionInterface;
use PhpExtended\Version\VersionParser;
use Psr\Http\Message\UriInterface;
use ReflectionClass;
use ReflectionException;

/**
 * ReifierConfiguration class file.
 * 
 * This class is a simple implementation of the ReifierConfigurationInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.LongVariable")
 * @SuppressWarnings("PHPMD.TooManyMethods")
 */
class ReifierConfiguration implements ReifierConfigurationInterface
{
	
	/**
	 * Whether to do class name inference by removing the prefix "I" of
	 * interface class names.
	 * 
	 * @var boolean
	 */
	protected bool $_inferImplementationClassNameFromIPrefix = true;
	
	/**
	 * Whether to do class name inference by removing the suffix "Interface" of
	 * interface class names.
	 * 
	 * @var boolean
	 */
	protected bool $_inferImplementationClassNameFromInterfaceSuffix = true;
	
	/**
	 * The polymorphism key value resolution mecanism.
	 * 
	 * [mother-class => [priority => [key => [value => child-class]]]]
	 * 
	 * @var array<class-string, array<integer, array<string, array<string, class-string>>>>
	 */
	protected array $_polymorphisms = [];
	
	/**
	 * The polymorphism presence resolution mecanism.
	 * 
	 * [mother-class => [priority => [key => child-class]]]
	 * 
	 * @var array<class-string, array<integer, array<string, class-string>>>
	 */
	protected array $_polymorphismPresences = [];
	
	/**
	 * The fields to ignore. [classname => [fieldname => 1]].
	 * 
	 * @var array<class-string, array<string, int>>
	 */
	protected array $_ignoreExcessFields = [];
	
	/**
	 * The fields that are allowed to fail. [classname => [fieldname => 1]].
	 * 
	 * @var array<class-string, array<string, int>>
	 */
	protected array $_allowedToFailFields = [];
	
	/**
	 * The fields that are to be interpreted as null if they are empty.
	 * [classname => [fieldname => 1]].
	 * 
	 * @var array<class-string, array<string, int>>
	 */
	protected array $_emptyToNullFields = [];
	
	/**
	 * The aliases of the fields names [classname => [fieldname => alias]].
	 * 
	 * @var array<class-string, array<string, string>>
	 */
	protected array $_aliases = [];
	
	/**
	 * The field names from aliases [classname => [alias => fieldname]].
	 * 
	 * @var array<class-string, array<string, string>>
	 */
	protected array $_fieldNames = [];
	
	/**
	 * The datetime formats. [classname => [fieldname => formats]].
	 * 
	 * @var array<class-string, array<string, array<integer, string>>>
	 */
	protected array $_datetimeFormats = [];
	
	/**
	 * The iterable inner types. [classname => [fieldname => innerclassname]].
	 * 
	 * @var array<class-string, array<string, class-string|'bool'|'int'|'float'|'string'|'array'>>
	 */
	protected array $_iterableInnerTypes = [];
	
	/**
	 * The lazy factories for parsers. [classname<T> => callable():Parser<T>].
	 * 
	 * @var array<class-string, callable():ParserInterface>
	 * @phpstan-ignore-next-line
	 */
	protected array $_parserCbs = [];
	
	/**
	 * The parsers. [classname => Parser].
	 * 
	 * @var array<class-string, ParserInterface>
	 * @phpstan-ignore-next-line
	 */
	protected array $_parsers = [];
	
	/**
	 * The implementations. [abstraction => implementation].
	 * 
	 * @var array<class-string, class-string>
	 */
	protected $_implementations = [
		AcceptLanguageChainInterface::class => AcceptLanguageChain::class,
		DateTimeInterface::class => DateTimeImmutable::class,
		EmailAddressInterface::class => EmailAddress::class,
		EmailAddressListInterface::class => EmailAddressList::class,
		IteratorAggregate::class => ArrayIteratorAggregate::class,
		Ipv4AddressInterface::class => Ipv4Address::class,
		Ipv4NetworkInterface::class => Ipv4Network::class,
		Ipv6AddressInterface::class => Ipv6Address::class,
		Ipv6NetworkInterface::class => Ipv6Network::class,
		LdapDistinguishedNameInterface::class => LdapDistinguishedName::class,
		LdapFilterNodeInterface::class => LdapFilterNodeMulti::class,
		LdapFilterNodeMultiInterface::class => LdapFilterNodeMulti::class,
		MacAddress48BitsInterface::class => MacAddress48Bits::class,
		MacAddress64BitsInterface::class => MacAddress64Bits::class,
		MailboxInterface::class => Mailbox::class,
		MailboxListInterface::class => MailboxList::class,
		MailboxGroupInterface::class => MailboxGroup::class,
		MailboxGroupListInterface::class => MailboxGroupList::class,
		MimeTypeInterface::class => MimeType::class,
		UriInterface::class => Uri::class,
		UlidInterface::class => Ulid::class,
		UuidInterface::class => Uuid::class,
		VersionInterface::class => Version::class,
	];
	
	/**
	 * Builds a new ReifierConfiguration with some basic insight with core
	 * classes and interfaces, and native types.
	 * 
	 * @psalm-suppress PropertyTypeCoercion
	 */
	public function __construct()
	{
		$this->_parserCbs[AcceptLanguageChain::class] = function() : ParserInterface
		{
			return new AcceptLanguageChainParser();
		};
		$this->_parserCbs['DateTime'] = function() : ParserInterface
		{
			// cant use DateTime::class because phpcs transforms it to DateTimeImmutable
			return new DateTimeParser();
		};
		$this->_parserCbs[DateTimeImmutable::class] = function() : ParserInterface
		{
			return new DateTimeImmutableParser();
		};
		$this->_parserCbs[EmailAddress::class] = function() : ParserInterface
		{
			return new EmailAddressParser();
		};
		$this->_parserCbs[EmailAddressList::class] = function() : ParserInterface
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
			return new EmailAddressListParser($this->getParser(EmailAddress::class));
		};
		$this->_parserCbs[Ipv4Address::class] = function() : ParserInterface
		{
			return new Ipv4AddressParser();
		};
		$this->_parserCbs[Ipv4Network::class] = function() : ParserInterface
		{
			return new Ipv4NetworkParser();
		};
		$this->_parserCbs[Ipv6Address::class] = function() : ParserInterface
		{
			return new Ipv6AddressParser();
		};
		$this->_parserCbs[Ipv6Network::class] = function() : ParserInterface
		{
			return new Ipv6NetworkParser();
		};
		$this->_parserCbs[LdapDistinguishedName::class] = function() : ParserInterface
		{
			return new LdapDistinguishedNameParser();
		};
		$this->_parserCbs[LdapFilterNodeMulti::class] = function() : ParserInterface
		{
			return new LdapFilterParser();
		};
		$this->_parserCbs[MacAddress48Bits::class] = function() : ParserInterface
		{
			return new MacAddress48Parser();
		};
		$this->_parserCbs[MacAddress64Bits::class] = function() : ParserInterface
		{
			return new MacAddress64Parser();
		};
		$this->_parserCbs[Mailbox::class] = function() : ParserInterface
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
			return new MailboxParser($this->getParser(EmailAddress::class));
		};
		$this->_parserCbs[MailboxList::class] = function() : ParserInterface
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
			return new MailboxListParser($this->getParser(Mailbox::class));
		};
		$this->_parserCbs[MailboxGroup::class] = function() : ParserInterface
		{
			return new MailboxGroupParser();
		};
		$this->_parserCbs[MailboxGroupList::class] = function() : ParserInterface
		{
			return new MailboxGroupListParser();
		};
		$this->_parserCbs[MimeType::class] = function() : ParserInterface
		{
			return new MimeTypeParser();
		};
		$this->_parserCbs[Uri::class] = function() : ParserInterface
		{
			return new UriParser();
		};
		$this->_parserCbs[Ulid::class] = function() : ParserInterface
		{
			return new UlidParser();
		};
		$this->_parserCbs[Uuid::class] = function() : ParserInterface
		{
			return new UuidParser();
		};
		$this->_parserCbs[Version::class] = function() : ParserInterface
		{
			return new VersionParser();
		};
		$this->_parserCbs[VersionConstraintInterface::class] = function() : ParserInterface
		{
			return new VersionConstraintParser();
		};
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::enableInferImplementationClassnameFromIPrefix()
	 */
	public function enableInferImplementationClassnameFromIPrefix() : ReifierConfigurationInterface
	{
		$this->_inferImplementationClassNameFromIPrefix = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::disableInferImplementationClassnameFromIPrefix()
	 */
	public function disableInferImplementationClassnameFromIPrefix() : ReifierConfigurationInterface
	{
		$this->_inferImplementationClassNameFromIPrefix = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::isInferImplementationClassnameFromIPrefixEnabled()
	 */
	public function isInferImplementationClassnameFromIPrefixEnabled() : bool
	{
		return $this->_inferImplementationClassNameFromIPrefix;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::enableInferImplementationClassnameFromInterfaceSuffix()
	 */
	public function enableInferImplementationClassnameFromInterfaceSuffix() : ReifierConfigurationInterface
	{
		$this->_inferImplementationClassNameFromInterfaceSuffix = true;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::disableInferImplementationClassnameFromInterfaceSuffix()
	 */
	public function disableInferImplementationClassnameFromInterfaceSuffix() : ReifierConfigurationInterface
	{
		$this->_inferImplementationClassNameFromInterfaceSuffix = false;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::isInferImplementationClassnameFromInterfaceSuffixEnabled()
	 */
	public function isInferImplementationClassnameFromInterfaceSuffixEnabled() : bool
	{
		return $this->_inferImplementationClassNameFromInterfaceSuffix;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addIgnoreExcessFields()
	 */
	public function addIgnoreExcessFields(string $fqcn, array $fieldNames) : ReifierConfigurationInterface
	{
		foreach($fieldNames as $fieldName)
		{
			$this->_ignoreExcessFields[$fqcn][$fieldName] = 1;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addIgnoreExcessField()
	 */
	public function addIgnoreExcessField(string $fqcn, string $fieldName) : ReifierConfigurationInterface
	{
		$this->_ignoreExcessFields[$fqcn][$fieldName] = 1;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::mustIgnoreExcessField()
	 */
	public function mustIgnoreExcessField(string $fqcn, string $fieldName) : bool
	{
		return isset($this->_ignoreExcessFields[$fqcn][$fieldName]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllIgnoredExcessFields()
	 */
	public function getAllIgnoredExcessFields() : array
	{
		$ret = [];
		
		foreach($this->_ignoreExcessFields as $class => $value)
		{
			$ret[$class] = \array_keys($value);
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addFieldsAllowedToFail()
	 */
	public function addFieldsAllowedToFail(string $fqcn, array $fieldNames) : ReifierConfigurationInterface
	{
		foreach($fieldNames as $fieldName)
		{
			$this->_allowedToFailFields[$fqcn][$fieldName] = 1;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addFieldAllowedToFail()
	 */
	public function addFieldAllowedToFail(string $fqcn, string $fieldName) : ReifierConfigurationInterface
	{
		$this->_allowedToFailFields[$fqcn][$fieldName] = 1;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::isFieldAllowedToFail()
	 */
	public function isFieldAllowedToFail(string $fqcn, string $fieldName) : bool
	{
		return isset($this->_allowedToFailFields[$fqcn][$fieldName]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllFieldsAllowedToFail()
	 */
	public function getAllFieldsAllowedToFail() : array
	{
		$ret = [];
		
		foreach($this->_allowedToFailFields as $key => $value)
		{
			$ret[$key] = \array_keys($value);
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addFieldsEmptyAsNull()
	 */
	public function addFieldsEmptyAsNull(string $fqcn, array $fieldNames) : ReifierConfigurationInterface
	{
		foreach($fieldNames as $fieldName)
		{
			$this->_emptyToNullFields[$fqcn][$fieldName] = 1;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addFieldEmptyAsNull()
	 */
	public function addFieldEmptyAsNull(string $fqcn, string $fieldName) : ReifierConfigurationInterface
	{
		$this->_emptyToNullFields[$fqcn][$fieldName] = 1;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::isFieldEmptyAsNull()
	 */
	public function isFieldEmptyAsNull(string $fqcn, string $fieldName) : bool
	{
		return isset($this->_emptyToNullFields[$fqcn][$fieldName]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllFieldsEmptyAsNull()
	 */
	public function getAllFieldsEmptyAsNull() : array
	{
		$ret = [];
		
		foreach($this->_emptyToNullFields as $key => $value)
		{
			$ret[$key] = \array_keys($value);
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addFieldNameAlias()
	 */
	public function addFieldNameAlias(string $fqcn, string $fieldName, string $alias) : ReifierConfigurationInterface
	{
		$this->_aliases[$fqcn][$fieldName] = $alias;
		$this->_fieldNames[$fqcn][$alias] = $fieldName;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getFieldNameAlias()
	 */
	public function getAliasFromFieldName(string $fqcn, string $fieldName) : string
	{
		return $this->_aliases[$fqcn][$fieldName] ?? $fieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAliasFromFieldName()
	 */
	public function getFieldNameFromAlias(string $fqcn, string $alias) : string
	{
		return $this->_fieldNames[$fqcn][$alias] ?? $alias;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllFieldNameAliases()
	 */
	public function getAllFieldNameAliases() : array
	{
		return $this->_aliases;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addDateTimeFormats()
	 */
	public function addDateTimeFormats(string $fqcn, array $fieldNames, array $formats) : ReifierConfigurationInterface
	{
		foreach($fieldNames as $fieldName)
		{
			$this->_datetimeFormats[$fqcn][$fieldName] = $formats;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addDateTimeFormat()
	 */
	public function addDateTimeFormat(string $fqcn, string $fieldName, array $formats) : ReifierConfigurationInterface
	{
		$this->_datetimeFormats[$fqcn][$fieldName] = $formats;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getDateTimeFormats()
	 */
	public function getDateTimeFormats(string $fqcn, string $fieldName) : array
	{
		return $this->_datetimeFormats[$fqcn][$fieldName] ?? [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllDateTimeFormats()
	 */
	public function getAllDateTimeFormats() : array
	{
		return $this->_datetimeFormats;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::setIterableInnerTypes()
	 */
	public function setIterableInnerTypes(string $fqcn, array $fieldNames, string $attributeInnerClass) : ReifierConfigurationInterface
	{
		foreach($fieldNames as $fieldName)
		{
			$this->_iterableInnerTypes[$fqcn][$fieldName] = $attributeInnerClass;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::setIterableInnerType()
	 */
	public function setIterableInnerType(string $fqcn, string $fieldName, string $attributeInnerClass) : ReifierConfigurationInterface
	{
		$this->_iterableInnerTypes[$fqcn][$fieldName] = $attributeInnerClass;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getIterableInnerType()
	 */
	public function getIterableInnerType(string $fqcn, string $fieldName) : string
	{
		if(isset($this->_iterableInnerTypes[$fqcn][$fieldName]))
		{
			return $this->_iterableInnerTypes[$fqcn][$fieldName];
		}
		
		throw new MissingInnerTypeException(null, 0, $fqcn, $fieldName, '.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllIterableInnerTypes()
	 */
	public function getAllIterableInnerTypes() : array
	{
		return $this->_iterableInnerTypes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addPolymorphism()
	 */
	public function addPolymorphism(string $fqcn, int $priority, string $key, string $value, string $innerClassFqcn) : ReifierConfigurationInterface
	{
		/** @psalm-suppress DocblockTypeContradiction */
		if(!\is_a($innerClassFqcn, $fqcn, true))
		{
			throw new InvalidArgumentException('The inner class '.$innerClassFqcn.' does not extends '.((string) $fqcn));
		}
		
		$this->_polymorphisms[$fqcn][$priority][$key][$value] = $innerClassFqcn;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getPolyMorphismsFor()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getPolyMorphismsFor(string $fqcn) : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->_polymorphisms[$fqcn] ?? [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllPolymorphisms()
	 */
	public function getAllPolymorphisms() : array
	{
		return $this->_polymorphisms;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::addPolymorphismPresence()
	 */
	public function addPolymorphismPresence(string $fqcn, int $priority, string $key, string $innerClassFqcn) : ReifierConfigurationInterface
	{
		/** @psalm-suppress DocblockTypeContradiction */
		if(!\is_a($innerClassFqcn, $fqcn, true))
		{
			throw new InvalidArgumentException('The inner class '.$innerClassFqcn.' does not extends '.((string) $fqcn));
		}
		
		$this->_polymorphismPresences[$fqcn][$priority][$key] = $innerClassFqcn;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getPolymorphismsPresenceFor()
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getPolymorphismsPresenceFor(string $fqcn) : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->_polymorphismPresences[$fqcn] ?? [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllPolymorphismsPresence()
	 */
	public function getAllPolymorphismsPresence() : array
	{
		return $this->_polymorphismPresences;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::setParser()
	 */
	public function setParser(string $fqcn, ParserInterface $parser) : ReifierConfigurationInterface
	{
		$this->_parsers[$fqcn] = $parser;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getParser()
	 * @psalm-suppress InvalidReturnType
	 */
	public function getParser(string $fqcn) : ParserInterface
	{
		if(isset($this->_parsers[$fqcn]))
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $this->_parsers[$fqcn];
		}
		
		if(isset($this->_parserCbs[$fqcn]))
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $this->_parsers[$fqcn] = $this->_parserCbs[$fqcn]();
		}
		
		try
		{
			$ifqcn = $this->getImplementation($fqcn);
		}
		catch(MissingImplementationThrowable $e)
		{
			throw new MissingParserException($e->getData(), $e->getDepths(), $fqcn, $e->getFailedAttribute(), '.', null, -1, $e);
		}
		
		if(isset($this->_parsers[$ifqcn]))
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $this->_parsers[$ifqcn];
		}
		
		if(isset($this->_parserCbs[$ifqcn]))
		{
			/** @psalm-suppress InvalidReturnStatement */
			return $this->_parsers[$ifqcn] = $this->_parserCbs[$ifqcn]();
		}
		
		throw new MissingParserException(null, 0, $fqcn, '', '.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllParsers()
	 * @psalm-suppress InvalidReturnType
	 */
	public function getAllParsers() : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->_parsers;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::setImplementation()
	 */
	public function setImplementation(string $interfaceFqcn, string $classFqcn) : ReifierConfigurationInterface
	{
		$rclass = $this->getReflectionClassInterface($interfaceFqcn);
		$aclass = $this->getReflectionClassObject($classFqcn);
		
		if($rclass->isInterface())
		{
			/** @psalm-suppress TypeDoesNotContainType */
			if(!$aclass->implementsInterface($interfaceFqcn))
			{
				$message = 'The given class {class} does not implements interface {interface}';
				$context = ['{class}' => $classFqcn, '{interface}' => $interfaceFqcn];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
		}
		
		if($rclass->isAbstract())
		{
			if(!$aclass->isSubclassOf($interfaceFqcn))
			{
				$message = 'The given class {class} does not extends abstract class {interface}';
				$context = ['{class}' => $classFqcn, '{interface}' => $interfaceFqcn];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
		}
		
		$this->_implementations[$interfaceFqcn] = $classFqcn;
		
		return $this;
	}
	
	/**
	 * Gets the suitable reflection class for the interface class name.
	 * 
	 * @template T of object
	 * @param class-string<T> $interfaceFqcn
	 * @return ReflectionClass<T>
	 * @throws InvalidArgumentException
	 */
	public function getReflectionClassInterface(string $interfaceFqcn) : ReflectionClass
	{
		try
		{
			$rclass = new ReflectionClass($interfaceFqcn);
		}
		/** @phpstan-ignore-next-line */ // dead catch
		catch(ReflectionException $e)
		{
			$message = 'Failed to get reflection class on {class}';
			$context = ['{class}' => $interfaceFqcn];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!($rclass->isAbstract() || $rclass->isInterface()))
		{
			$message = 'The given class {class} is not abstract nor an interface.';
			$context = ['{class}' => $interfaceFqcn];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $rclass;
	}
	
	/**
	 * Gets the suitable reflection class for the object class name.
	 * 
	 * @template T of object
	 * @param class-string<T> $classFqcn
	 * @return ReflectionClass<T>
	 * @throws InvalidArgumentException
	 */
	public function getReflectionClassObject(string $classFqcn) : ReflectionClass
	{
		try
		{
			$aclass = new ReflectionClass($classFqcn);
		}
		/** @phpstan-ignore-next-line */ // dead catch
		catch(ReflectionException $e)
		{
			$message = 'Failed to get reflection class on {class}';
			$context = ['{class}' => $classFqcn];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if($aclass->isAbstract() || $aclass->isInterface())
		{
			$message = 'The given class {class} is abstract or is an interface and cannot be used as target to reification.';
			$context = ['{class}' => $classFqcn];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $aclass;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getImplementation()
	 * @psalm-suppress MoreSpecificReturnType
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function getImplementation(string $interfaceFqcn) : string
	{
		if(isset($this->_implementations[$interfaceFqcn]))
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
			return $this->_implementations[$interfaceFqcn];
		}
		
		try
		{
			$rclass = new ReflectionClass($interfaceFqcn);
		}
		/** @phpstan-ignore-next-line */ // dead catch
		catch(ReflectionException $e)
		{
			$message = 'Failed to get reflection class on {class}';
			$context = ['{class}' => $interfaceFqcn];
			
			throw new MissingImplementationException(null, 0, $interfaceFqcn, '', '.', \strtr($message, $context), -1, $e);
		}
		
		if(!$rclass->isAbstract() && !$rclass->isInterface())
		{
			return $this->_implementations[$interfaceFqcn] = $interfaceFqcn;
		}
		
		if($this->isInferImplementationClassnameFromIPrefixEnabled())
		{
			$classFqcnParts = \explode('\\', $interfaceFqcn);
			$lastIndex = \count($classFqcnParts) - 1;
			$lastPart = $classFqcnParts[$lastIndex];
			if(\preg_match('#^I[A-Z]#', $lastPart))
			{
				$newLastPart = \substr($lastPart, 1);
				$classFqcnParts[$lastIndex] = $newLastPart;
				$classFqcn = \implode('\\', $classFqcnParts);
				if(\class_exists($classFqcn))
				{
					$rclass = new ReflectionClass($classFqcn);
					if(!$rclass->isAbstract() && !$rclass->isInterface() && $rclass->implementsInterface($interfaceFqcn))
					{
						/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
						return $this->_implementations[$interfaceFqcn] = $classFqcn;
					}
				}
			}
		}
		
		if($this->isInferImplementationClassnameFromInterfaceSuffixEnabled())
		{
			$classFqcnParts = \explode('\\', $interfaceFqcn);
			$lastIndex = \count($classFqcnParts) - 1;
			$lastPart = $classFqcnParts[$lastIndex];
			if(\preg_match('#Interface$#', $lastPart))
			{
				$newLastPart = \substr($lastPart, 0, -9);
				$classFqcnParts[$lastIndex] = $newLastPart;
				$classFqcn = \implode('\\', $classFqcnParts);
				if(\class_exists($classFqcn))
				{
					$rclass = new ReflectionClass($classFqcn);
					if(!$rclass->isAbstract() && !$rclass->isInterface() && $rclass->implementsInterface($interfaceFqcn))
					{
						/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
						return $this->_implementations[$interfaceFqcn] = $classFqcn;
					}
				}
			}
		}
		
		throw new MissingImplementationException(null, 0, $interfaceFqcn, '', '.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::getAllImplementations()
	 */
	public function getAllImplementations() : array
	{
		return $this->_implementations;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ReifierConfigurationInterface::mergeWith()
	 * @throws InvalidArgumentException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function mergeWith(?ReifierConfigurationInterface $other) : ReifierConfigurationInterface
	{
		if(null === $other)
		{
			return $this;
		}
		
		$new = new self();
		$other->isInferImplementationClassnameFromIPrefixEnabled() ? $new->enableInferImplementationClassnameFromIPrefix() : $new->disableInferImplementationClassnameFromIPrefix();
		$other->isInferImplementationClassnameFromInterfaceSuffixEnabled() ? $new->enableInferImplementationClassnameFromInterfaceSuffix() : $new->disableInferImplementationClassnameFromInterfaceSuffix();
		
		foreach($this->getAllDateTimeFormats() as $fqcn => $value)
		{
			foreach($value as $name => $formats)
			{
				$new->addDateTimeFormats($fqcn, [$name], $formats);
			}
		}
			
		foreach($other->getAllDateTimeFormats() as $fqcn => $value)
		{
			foreach($value as $name => $formats)
			{
				$new->addDateTimeFormats($fqcn, [$name], $formats);
			}
		}
		
		foreach($this->getAllFieldNameAliases() as $fqcn => $value)
		{
			foreach($value as $fieldName => $alias)
			{
				$new->addFieldNameAlias($fqcn, $fieldName, $alias);
			}
		}
		
		foreach($other->getAllFieldNameAliases() as $fqcn => $value)
		{
			foreach($value as $fieldName => $alias)
			{
				$new->addFieldNameAlias($fqcn, $fieldName, $alias);
			}
		}
		
		foreach($this->getAllFieldsEmptyAsNull() as $fqcn => $fieldNames)
		{
			$new->addFieldsEmptyAsNull($fqcn, $fieldNames);
		}
		
		foreach($other->getAllFieldsEmptyAsNull() as $fqcn => $fieldNames)
		{
			$new->addFieldsEmptyAsNull($fqcn, $fieldNames);
		}
		
		foreach($this->getAllFieldsAllowedToFail() as $fqcn => $fieldNames)
		{
			$new->addFieldsAllowedToFail($fqcn, $fieldNames);
		}
		
		foreach($other->getAllFieldsAllowedToFail() as $fqcn => $fieldNames)
		{
			$new->addFieldsAllowedToFail($fqcn, $fieldNames);
		}
		
		foreach($this->getAllIgnoredExcessFields() as $fqcn => $fieldNames)
		{
			$new->addIgnoreExcessFields($fqcn, $fieldNames);
		}
		
		foreach($other->getAllIgnoredExcessFields() as $fqcn => $fieldNames)
		{
			$new->addIgnoreExcessFields($fqcn, $fieldNames);
		}
		
		foreach($this->getAllImplementations() as $interfaceFqcn => $classFqcn)
		{
			// when classes are cached as their own implementation
			if($interfaceFqcn !== $classFqcn)
			{
				$new->setImplementation($interfaceFqcn, $classFqcn);
			}
		}
		
		foreach($other->getAllImplementations() as $interfaceFqcn => $classFqcn)
		{
			// when classes are cached as their own implementation
			if($interfaceFqcn !== $classFqcn)
			{
				$new->setImplementation($interfaceFqcn, $classFqcn);
			}
		}
		
		foreach($this->getAllIterableInnerTypes() as $fqcn => $value)
		{
			foreach($value as $fieldName => $attributeInnerClass)
			{
				$new->setIterableInnerTypes($fqcn, [$fieldName], $attributeInnerClass);
			}
		}
		
		foreach($other->getAllIterableInnerTypes() as $fqcn => $value)
		{
			foreach($value as $fieldName => $attributeInnerClass)
			{
				$new->setIterableInnerTypes($fqcn, [$fieldName], $attributeInnerClass);
			}
		}
		
		foreach($this->getAllParsers() as $fqcn => $parser)
		{
			/** @psalm-suppress InvalidArgument */
			$new->setParser($fqcn, $parser);
		}
		
		foreach($other->getAllParsers() as $fqcn => $parser)
		{
			/** @psalm-suppress InvalidArgument */
			$new->setParser($fqcn, $parser);
		}
		
		foreach($this->getAllPolymorphisms() as $motherClass => $p1)
		{
			foreach($p1 as $priority => $p3)
			{
				foreach($p3 as $key => $p4)
				{
					foreach($p4 as $value => $childClass)
					{
						$new->addPolymorphism($motherClass, $priority, $key, $value, $childClass);
					}
				}
			}
		}
		
		foreach($other->getAllPolymorphisms() as $motherClass => $p1)
		{
			foreach($p1 as $priority => $p3)
			{
				foreach($p3 as $key => $p4)
				{
					foreach($p4 as $value => $childClass)
					{
						$new->addPolymorphism($motherClass, $priority, $key, $value, $childClass);
					}
				}
			}
		}
		
		foreach($this->getAllPolymorphismsPresence() as $motherClass => $p1)
		{
			foreach($p1 as $priority => $p3)
			{
				foreach($p3 as $key => $childClass)
				{
					$new->addPolymorphismPresence($motherClass, $priority, $key, $childClass);
				}
			}
		}
		
		foreach($other->getAllPolymorphismsPresence() as $motherClass => $p1)
		{
			foreach($p1 as $priority => $p3)
			{
				foreach($p3 as $key => $childClass)
				{
					$new->addPolymorphismPresence($motherClass, $priority, $key, $childClass);
				}
			}
		}
		
		return $new;
	}
	
}
