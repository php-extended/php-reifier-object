<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

/**
 * ObjectFactoryIgnore class file.
 * 
 * This class is a link that handles remaining data that could not be
 * transformed previously to check if this is an expected or unexpected error.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends ObjectFactoryLink<T>
 */
class ObjectFactoryIgnore extends ObjectFactoryLink
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Reifier\ObjectFactoryLink::applyTo()
	 */
	public function applyTo($object, array $data, int $depths, string $path, ReifierConfigurationInterface $config) : object
	{
		if(null === $object)
		{
			return parent::applyTo($object, $data, $depths, $path, $config);
		}
		
		$failedToBeProcessed = [];
		
		foreach(\array_keys($data) as $key)
		{
			if(!$config->mustIgnoreExcessField($this->_rclass->getName(), (string) $key))
			{
				$failedToBeProcessed[] = $key;
			}
		}
		
		if(!empty($failedToBeProcessed))
		{
			$message = 'The attributes "{attrlist}" failed to be processed by any of the setting methods.';
			$context = ['{attrlist}' => \implode(', ', $failedToBeProcessed)];
			
			throw new ReificationException($data, $depths, $this->_rclass->getName(), $context['{attrlist}'], $path, \strtr($message, $context));
		}
		
		return parent::applyTo($object, [], $depths, $path, $config);
	}
	
}
