<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTime\DateTimeImmutableParser;
use PhpExtended\DateTime\DateTimeParser;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailAddressListInterface;
use PhpExtended\Email\EmailAddressListParser;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxGroupInterface;
use PhpExtended\Email\MailboxGroupList;
use PhpExtended\Email\MailboxGroupListInterface;
use PhpExtended\Email\MailboxGroupListParser;
use PhpExtended\Email\MailboxGroupParser;
use PhpExtended\Email\MailboxInterface;
use PhpExtended\Email\MailboxList;
use PhpExtended\Email\MailboxListInterface;
use PhpExtended\Email\MailboxListParser;
use PhpExtended\Email\MailboxParser;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv4AddressParser;
use PhpExtended\Ip\Ipv4Network;
use PhpExtended\Ip\Ipv4NetworkInterface;
use PhpExtended\Ip\Ipv4NetworkParser;
use PhpExtended\Ip\Ipv6Address;
use PhpExtended\Ip\Ipv6AddressInterface;
use PhpExtended\Ip\Ipv6AddressParser;
use PhpExtended\Ip\Ipv6Network;
use PhpExtended\Ip\Ipv6NetworkInterface;
use PhpExtended\Ip\Ipv6NetworkParser;
use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameInterface;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapFilterNodeInterface;
use PhpExtended\Ldap\LdapFilterNodeMulti;
use PhpExtended\Ldap\LdapFilterNodeMultiInterface;
use PhpExtended\Ldap\LdapFilterParser;
use PhpExtended\Mac\MacAddress48Bits;
use PhpExtended\Mac\MacAddress48BitsInterface;
use PhpExtended\Mac\MacAddress48Parser;
use PhpExtended\Mac\MacAddress64Bits;
use PhpExtended\Mac\MacAddress64BitsInterface;
use PhpExtended\Mac\MacAddress64Parser;
use PhpExtended\MimeType\MimeType;
use PhpExtended\MimeType\MimeTypeInterface;
use PhpExtended\MimeType\MimeTypeParser;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParserInterface;
use PhpExtended\Reifier\MissingImplementationException;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\MissingParserException;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PhpExtended\Ulid\Ulid;
use PhpExtended\Ulid\UlidInterface;
use PhpExtended\Ulid\UlidParser;
use PhpExtended\Uri\UriParser;
use PhpExtended\Uuid\Uuid;
use PhpExtended\Uuid\UuidInterface;
use PhpExtended\Uuid\UuidParser;
use PhpExtended\Version\Version;
use PhpExtended\Version\VersionConstraintInterface;
use PhpExtended\Version\VersionConstraintParser;
use PhpExtended\Version\VersionInterface;
use PhpExtended\Version\VersionParser;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriInterface;

/**
 * ReifierConfigurationTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierConfiguration
 *
 * @internal
 *
 * @small
 */
class ReifierConfigurationTest extends TestCase
{
	
	/**
	 * The configuration to test.
	 * 
	 * @var ReifierConfiguration
	 */
	protected ReifierConfiguration $_config;
	
	public function testToString() : void
	{
		$object = $this->_config;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testInferImplementationClassnameWithIPrefix() : void
	{
		$this->assertTrue($this->_config->isInferImplementationClassnameFromIPrefixEnabled());
		$this->_config->disableInferImplementationClassnameFromIPrefix();
		$this->assertFalse($this->_config->isInferImplementationClassnameFromIPrefixEnabled());
		$this->_config->enableInferImplementationClassnameFromIPrefix();
		$this->assertTrue($this->_config->isInferImplementationClassnameFromIPrefixEnabled());
	}
	
	public function testInferImplementationClassnameWithInterfaceSuffix() : void
	{
		$this->assertTrue($this->_config->isInferImplementationClassnameFromInterfaceSuffixEnabled());
		$this->_config->disableInferImplementationClassnameFromInterfaceSuffix();
		$this->assertFalse($this->_config->isInferImplementationClassnameFromInterfaceSuffixEnabled());
		$this->_config->enableInferImplementationClassnameFromInterfaceSuffix();
		$this->assertTrue($this->_config->isInferImplementationClassnameFromInterfaceSuffixEnabled());
	}
	
	public function testIgnoreExcessFields() : void
	{
		$this->assertFalse($this->_config->mustIgnoreExcessField(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addIgnoreExcessFields(ReifierConfigurationTest::class, ['testfield']);
		$this->assertTrue($this->_config->mustIgnoreExcessField(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testIgnoreExcessField() : void
	{
		$this->assertFalse($this->_config->mustIgnoreExcessField(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addIgnoreExcessField(ReifierConfigurationTest::class, 'testfield');
		$this->assertTrue($this->_config->mustIgnoreExcessField(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testAllowedToFailFields() : void
	{
		$this->assertFalse($this->_config->isFieldAllowedToFail(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addFieldsAllowedToFail(ReifierConfigurationTest::class, ['testfield']);
		$this->assertTrue($this->_config->isFieldAllowedToFail(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testAllowedToFailField() : void
	{
		$this->assertFalse($this->_config->isFieldAllowedToFail(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addFieldAllowedToFail(ReifierConfigurationTest::class, 'testfield');
		$this->assertTrue($this->_config->isFieldAllowedToFail(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testEmptyAsNullFields() : void
	{
		$this->assertFalse($this->_config->isFieldEmptyAsNull(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addFieldsEmptyAsNull(ReifierConfigurationTest::class, ['testfield']);
		$this->assertTrue($this->_config->isFieldEmptyAsNull(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testEmptyAsNullField() : void
	{
		$this->assertFalse($this->_config->isFieldEmptyAsNull(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addFieldEmptyAsNull(ReifierConfigurationTest::class, 'testfield');
		$this->assertTrue($this->_config->isFieldEmptyAsNull(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testAliases() : void
	{
		$this->assertEquals($this->_config->getAliasFromFieldName(ReifierConfigurationTest::class, 'testfield'), 'testfield');
		$this->_config->addFieldNameAlias(ReifierConfigurationTest::class, 'testfield', 'testalias');
		$this->assertEquals($this->_config->getAliasFromFieldName(ReifierConfigurationTest::class, 'testfield'), 'testalias');
	}
	
	public function testFieldNameFromAlias() : void
	{
		$this->assertEquals($this->_config->getFieldNameFromAlias(ReifierConfigurationTest::class, 'testfield'), 'testfield');
		$this->_config->addFieldNameAlias(ReifierConfigurationTest::class, 'testfield', 'testalias');
		$this->assertEquals($this->_config->getFieldNameFromAlias(ReifierConfigurationTest::class, 'testalias'), 'testfield');
	}
	
	public function testPolymorphism() : void
	{
		$this->assertEmpty($this->_config->getPolyMorphismsFor(ReifierConfigurationTest::class));
		$this->_config->addPolymorphism(ReifierConfigurationTest::class, 0, 'polykey', 'polyvalue', ReifierConfigurationTest::class);
		$this->assertNotEmpty($this->_config->getPolyMorphismsFor(ReifierConfigurationTest::class));
	}
	
	public function testPolymorphismFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		$this->_config->addPolymorphism(Ipv4AddressInterface::class, 0, 'key', 'value', EmailAddress::class);
	}
	
	public function testPolymorphismPresence() : void
	{
		$this->assertEmpty($this->_config->getPolymorphismsPresenceFor(ReifierConfigurationTest::class));
		$this->_config->addPolymorphismPresence(ReifierConfigurationTest::class, 0, 'pkey', ReifierConfigurationTest::class);
		$this->assertNotEmpty($this->_config->getPolymorphismsPresenceFor(ReifierConfigurationTest::class));
	}
	
	public function testPolymorphismPresenceFailed() : void
	{
		$this->expectException(InvalidArgumentException::class);
		$this->_config->addPolymorphismPresence(Ipv4AddressInterface::class, 0, 'key', EmailAddress::class);
	}
	
	public function testDateTimesFormat() : void
	{
		$this->assertEquals([], $this->_config->getDateTimeFormats(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addDateTimeFormats(ReifierConfigurationTest::class, ['testfield'], ['!Y-m-d']);
		$this->assertEquals(['!Y-m-d'], $this->_config->getDateTimeFormats(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testDateTimeFormat() : void
	{
		$this->assertEquals([], $this->_config->getDateTimeFormats(ReifierConfigurationTest::class, 'testfield'));
		$this->_config->addDateTimeFormat(ReifierConfigurationTest::class, 'testfield', ['!Y-m-d']);
		$this->assertEquals(['!Y-m-d'], $this->_config->getDateTimeFormats(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testIterableInnerTypes() : void
	{
		$this->_config->setIterableInnerTypes(ReifierConfigurationTest::class, ['testfield'], DateTimeImmutable::class);
		$this->assertEquals(DateTimeImmutable::class, $this->_config->getIterableInnerType(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testIterableInnerType() : void
	{
		$this->_config->setIterableInnerType(ReifierConfigurationTest::class, 'testfield', DateTimeImmutable::class);
		$this->assertEquals(DateTimeImmutable::class, $this->_config->getIterableInnerType(ReifierConfigurationTest::class, 'testfield'));
	}
	
	public function testIterableInnerTypeMissing() : void
	{
		$this->expectException(MissingInnerTypeException::class);
		
		$this->_config->getIterableInnerType(ReifierConfigurationTest::class, 'testfield');
	}
	
	public function testParserAcceptLanguage() : void
	{
		$this->assertEquals(new AcceptLanguageChainParser(), $this->_config->getParser(AcceptLanguageChainInterface::class));
	}
	
	public function testParserDateTime() : void
	{
		$this->assertEquals(new DateTimeParser(), $this->_config->getParser('DateTime'));
	}
	
	public function testParserDateTimeInterface() : void
	{
		$this->assertEquals(new DateTimeImmutableParser(), $this->_config->getParser(DateTimeInterface::class));
	}
	
	public function testParserDateTimeImmutable() : void
	{
		$this->assertEquals(new DateTimeImmutableParser(), $this->_config->getParser(DateTimeImmutable::class));
	}
	
	public function testParserEmailAddressInterface() : void
	{
		$this->assertEquals(new EmailAddressParser(), $this->_config->getParser(EmailAddressInterface::class));
	}
	
	public function testParserEmailAddress() : void
	{
		$this->assertEquals(new EmailAddressParser(), $this->_config->getParser(EmailAddress::class));
	}
	
	public function testParserEmailAddressListInterface() : void
	{
		$this->assertEquals(new EmailAddressListParser(new EmailAddressParser()), $this->_config->getParser(EmailAddressListInterface::class));
	}
	
	public function testParserEmailAddressList() : void
	{
		$this->assertEquals(new EmailAddressListParser(new EmailAddressParser()), $this->_config->getParser(EmailAddressList::class));
	}
	
	public function testParserIpv4AddressInterface() : void
	{
		$this->assertEquals(new Ipv4AddressParser(), $this->_config->getParser(Ipv4AddressInterface::class));
	}
	
	public function testParserIpv4Address() : void
	{
		$this->assertEquals(new Ipv4AddressParser(), $this->_config->getParser(Ipv4Address::class));
	}
	
	public function testParserIpv4NetworkInterface() : void
	{
		$this->assertEquals(new Ipv4NetworkParser(), $this->_config->getParser(Ipv4NetworkInterface::class));
	}
	
	public function testParserIpv4Network() : void
	{
		$this->assertEquals(new Ipv4NetworkParser(), $this->_config->getParser(Ipv4Network::class));
	}
	
	public function testParserIpv6AddressInterface() : void
	{
		$this->assertEquals(new Ipv6AddressParser(), $this->_config->getParser(Ipv6AddressInterface::class));
	}
	
	public function testParserIpv6Address() : void
	{
		$this->assertEquals(new Ipv6AddressParser(), $this->_config->getParser(Ipv6Address::class));
	}
	
	public function testParserIpv6NetworkInterface() : void
	{
		$this->assertEquals(new Ipv6NetworkParser(), $this->_config->getParser(Ipv6NetworkInterface::class));
	}
	
	public function testParserIpv6Network() : void
	{
		$this->assertEquals(new Ipv6NetworkParser(), $this->_config->getParser(Ipv6Network::class));
	}
	
	public function testParserLdapDistinguishedNameInterface() : void
	{
		$this->assertEquals(new LdapDistinguishedNameParser(), $this->_config->getParser(LdapDistinguishedNameInterface::class));
	}
	
	public function testParserLdapDistinguishedName() : void
	{
		$this->assertEquals(new LdapDistinguishedNameParser(), $this->_config->getParser(LdapDistinguishedName::class));
	}
	
	public function testParserLdapFilterNodeInterface() : void
	{
		$this->assertEquals(new LdapFilterParser(), $this->_config->getParser(LdapFilterNodeInterface::class));
	}
	
	public function testParserLdapFilterNodeMultiInterface() : void
	{
		$this->assertEquals(new LdapFilterParser(), $this->_config->getParser(LdapFilterNodeMultiInterface::class));
	}
	
	public function testParserLdapFilterNodeMulti() : void
	{
		$this->assertEquals(new LdapFilterParser(), $this->_config->getParser(LdapFilterNodeMulti::class));
	}
	
	public function testParserMacAddress48BitsInterface() : void
	{
		$this->assertEquals(new MacAddress48Parser(), $this->_config->getParser(MacAddress48BitsInterface::class));
	}
	
	public function testParserMacAddress48Bits() : void
	{
		$this->assertEquals(new MacAddress48Parser(), $this->_config->getParser(MacAddress48Bits::class));
	}
	
	public function testParserMacAddress64BitsInterface() : void
	{
		$this->assertEquals(new MacAddress64Parser(), $this->_config->getParser(MacAddress64BitsInterface::class));
	}
	
	public function testParserMacAddress64Bits() : void
	{
		$this->assertEquals(new MacAddress64Parser(), $this->_config->getParser(MacAddress64Bits::class));
	}
	
	public function testParserMailboxInterface() : void
	{
		$this->assertEquals(new MailboxParser(new EmailAddressParser()), $this->_config->getParser(MailboxInterface::class));
	}
	
	public function testParserMailbox() : void
	{
		$this->assertEquals(new MailboxParser(new EmailAddressParser()), $this->_config->getParser(Mailbox::class));
	}
	
	public function testParserMailboxListInterface() : void
	{
		$this->assertEquals(new MailboxListParser(new MailboxParser(new EmailAddressParser())), $this->_config->getParser(MailboxListInterface::class));
	}
	
	public function testparserMailboxList() : void
	{
		$this->assertEquals(new MailboxListParser(new MailboxParser(new EmailAddressParser())), $this->_config->getParser(MailboxList::class));
	}
	
	public function testParserMailboxGroupInterface() : void
	{
		$this->assertEquals(new MailboxGroupParser(), $this->_config->getParser(MailboxGroupInterface::class));
	}
	
	public function testParserMailboxGroup() : void
	{
		$this->assertEquals(new MailboxGroupParser(), $this->_config->getParser(MailboxGroup::class));
	}
	
	public function testParserMailboxGroupListInterface() : void
	{
		$this->assertEquals(new MailboxGroupListParser(), $this->_config->getParser(MailboxGroupListInterface::class));
	}
	
	public function testParserMailboxGroupList() : void
	{
		$this->assertEquals(new MailboxGroupListParser(), $this->_config->getParser(MailboxGroupList::class));
	}
	
	public function testParserMimeTypeInterface() : void
	{
		$this->assertEquals(new MimeTypeParser(), $this->_config->getParser(MimeTypeInterface::class));
	}
	
	public function testParserMimeType() : void
	{
		$this->assertEquals(new MimeTypeParser(), $this->_config->getParser(MimeType::class));
	}
	
	public function testParserUriInterface() : void
	{
		$this->assertEquals(new UriParser(), $this->_config->getParser(UriInterface::class));
	}
	
	public function testParserUri() : void
	{
		$this->assertEquals(new UriParser(), $this->_config->getParser(Uri::class));
	}
	
	public function testParserUlidInterface() : void
	{
		$this->assertEquals(new UlidParser(), $this->_config->getParser(UlidInterface::class));
	}
	
	public function testParserUlid() : void
	{
		$this->assertEquals(new UlidParser(), $this->_config->getParser(Ulid::class));
	}
	
	public function testParserUuidInterface() : void
	{
		$this->assertEquals(new UuidParser(), $this->_config->getParser(UuidInterface::class));
	}
	
	public function testParserUuid() : void
	{
		$this->assertEquals(new UuidParser(), $this->_config->getParser(Uuid::class));
	}
	
	public function testParserVersionInterface() : void
	{
		$this->assertEquals(new VersionParser(), $this->_config->getParser(VersionInterface::class));
	}
	
	public function testParserVersion() : void
	{
		$this->assertEquals(new VersionParser(), $this->_config->getParser(Version::class));
	}
	
	public function testParserVersionConstraintInterface() : void
	{
		$this->assertEquals(new VersionConstraintParser(), $this->_config->getParser(VersionConstraintInterface::class));
	}
	
	public function testParser1() : void
	{
		$this->_config->setParser(DateTimeImmutable::class, new DateTimeImmutableParser());
		$this->assertEquals(new DateTimeImmutableParser(), $this->_config->getParser(DateTimeInterface::class));
	}
	
	public function testParser2() : void
	{
		$parser = new class() extends AbstractParser implements ParserInterface
		{
			public function __toString() : string
			{
				return self::class.'@'.\spl_object_hash($this);
			}
			
			public function parse(?string $data) : object
			{
				return $this;
			}
			
		};
		
		$this->_config->setParser(\get_class($parser), $parser);
		$this->assertEquals($parser, $this->_config->getParser(\get_class($parser)));
	}
	
	public function testParser3() : void
	{
		$this->expectException(MissingParserException::class);
		
		$this->_config->getParser(ReifierConfigurationTest::class);
	}
	
	public function testParser4() : void
	{
		$this->expectException(MissingParserException::class);
		
		$this->_config->getParser(Throwable::class);
	}
	
	public function testReflectionInterface() : void
	{
		$this->assertInstanceOf(ReflectionClass::class, $this->_config->getReflectionClassInterface(Throwable::class));
	}
	
	public function testReflectionInterfaceFails() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->getReflectionClassInterface(ArrayIterator::class);
	}
	
	public function testReflectionInterfaceNonexistant() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->getReflectionClassInterface('foobar');
	}
	
	public function testReflectionObject() : void
	{
		$this->assertInstanceOf(ReflectionClass::class, $this->_config->getReflectionClassObject(ArrayIterator::class));
	}
	
	public function testReflectionObjectFails() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->getReflectionClassObject(Throwable::class);
	}
	
	public function testReflectionObjectNonexistant() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->getReflectionClassObject('foobar');
	}
	
	public function testSetImplementationSuccess() : void
	{
		$this->assertEquals($this->_config, $this->_config->setImplementation(DateTimeInterface::class, DateTimeImmutable::class));
	}
	
	public function testSetImplementationFailsNotAbstractSubclass() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->setImplementation(TestCase::class, ArrayIterator::class);
	}
	
	public function testSetImplementationFailsNotImplementsSubclass() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_config->setImplementation(Throwable::class, ArrayIterator::class);
	}
	
	public function testGetImplementationNonexistant() : void
	{
		$this->expectException(MissingImplementationException::class);
		
		$this->_config->getImplementation('foobar');
	}
	
	public function testMergeWith() : void
	{
		$this->_config->addDateTimeFormats(ReifierConfigurationTest::class, ['test'], ['!Y-m-d']);
		$this->_config->addFieldNameAlias(ReifierConfigurationTest::class, 'test', 'alias');
		$this->_config->addIgnoreExcessField(ReifierConfigurationTest::class, 'test');
		$this->_config->addFieldAllowedToFail(ReifierConfigurationTest::class, 'test');
		$this->_config->addFieldEmptyAsNull(ReifierConfigurationTest::class, 'test');
		$this->_config->setImplementation(ReifierConfigurationInterface::class, ReifierConfiguration::class);
		$this->_config->setIterableInnerTypes(ReifierConfigurationTest::class, ['testfield'], ReifierConfigurationTest::class);
		$this->_config->setParser('dateAndTime', new DateTimeParser());
		$this->_config->addPolymorphism(Traversable::class, 0, 'polykey', 'polyvalue', Iterator::class);
		$this->_config->addPolymorphismPresence(Traversable::class, 0, 'pkey', Iterator::class);
		
		$config = new ReifierConfiguration();
		
		$config->addDateTimeFormats(ReifierConfigurationTest::class, ['test'], ['!Y-m-d']);
		$config->addFieldNameAlias(ReifierConfigurationTest::class, 'test', 'alias');
		$config->addIgnoreExcessField(ReifierConfigurationTest::class, 'test');
		$config->addFieldAllowedToFail(ReifierConfigurationTest::class, 'test');
		$config->addFieldEmptyAsNull(ReifierConfigurationTest::class, 'test');
		$config->setImplementation(ReifierConfigurationInterface::class, ReifierConfiguration::class);
		$config->setIterableInnerTypes(ReifierConfigurationTest::class, ['testfield'], ReifierConfigurationTest::class);
		$config->setParser('dateAndTime', new DateTimeParser());
		$config->addPolymorphism(Traversable::class, 0, 'polykey', 'polyvalue', Iterator::class);
		$config->addPolymorphismPresence(Traversable::class, 0, 'pkey', Iterator::class);
		
		$this->assertEquals($config, $this->_config->mergeWith($config));
	}
	
	public function testMergeNull() : void
	{
		$this->assertEquals($this->_config, $this->_config->mergeWith(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_config = new ReifierConfiguration();
	}
	
}
