<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryLink;
use PhpExtended\Reifier\Reifier;
use PHPUnit\Framework\TestCase;

/**
 * ObjectFactoryLinkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryLinkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ObjectFactoryLink
	 */
	protected ObjectFactoryLink $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'['.\get_class($this->_object).']@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCamelCase() : void
	{
		$this->assertEquals('portugueseBrazil', $this->_object->getCamelCaseName('Portuguese (Brazil)'));
	}
	
	public function testCamelCase2() : void
	{
		$this->assertEquals('iso31662Lvl4', $this->_object->getCamelCaseName('ISO3166-2-lvl4'));
	}
	
	public function testCamelCase3() : void
	{
		$this->assertEquals('field2Name', $this->_object->getCamelCaseName('field2_name'));
	}
	
	public function testCamelCase4() : void
	{
		$this->assertEquals('field2name', $this->_object->getCamelCaseName('field_2name'));
	}
	
	public function testCamelCase5() : void
	{
		$this->assertEquals('field2Name', $this->_object->getCamelCaseName('field_2_name'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ObjectFactoryLink(new Reifier(), new LooseEnsurer(), new ReflectionClass(ObjectFactoryLink::class));
	}
	
}
