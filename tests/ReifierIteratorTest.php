<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierIterator;
use PHPUnit\Framework\TestCase;

class ReifiableStandardObject
{
	public string $data;
}

/**
 * ReifierIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierIterator
 * @internal
 * @small
 */
class ReifierIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReifierIterator
	 */
	protected ReifierIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testReified() : void
	{
		$count = 0;
		$expected = new ReifiableStandardObject();
		$expected->data = 'toto';
		
		foreach($this->_object as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertEquals($expected, $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReifierIterator(new Reifier(), ReifiableStandardObject::class, new ArrayIterator([['data' => 'toto']]), false, false, null);
	}
	
}
