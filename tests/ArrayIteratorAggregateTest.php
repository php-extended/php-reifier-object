<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ArrayIteratorAggregate;
use PHPUnit\Framework\TestCase;

/**
 * ArrayIteratorAggregateTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ArrayIteratorAggregate
 *
 * @internal
 *
 * @small
 */
class ArrayIteratorAggregateTest extends TestCase
{
	
	/**
	 * The iterator to test.
	 * 
	 * @var ArrayIteratorAggregate
	 */
	protected ArrayIteratorAggregate $_iterator;
	
	public function testToString() : void
	{
		$object = $this->_iterator;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testIterator() : void
	{
		$this->assertEquals(new ArrayIterator([new stdClass()]), $this->_iterator->getIterator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_iterator = new ArrayIteratorAggregate([new stdClass()]);
	}
	
}
