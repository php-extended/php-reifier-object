<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\ObjectFactoryField;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestGuessPropBuildableObject
{
	
	public $bool;
	public $int;
	public $float;
	public $string;
	public $array;
	
	// no setters and getters to trigger the guess type from field name
	
}

/**
 * ObjectFactoryGuessPropTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryField
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryFieldGuessPropTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryField
	 */
	protected ObjectFactoryField $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'array' => [true, false],
		];
		
		$this->_config->setIterableInnerTypes(TestGuessPropBuildableObject::class, ['array'], 'bool');
		/** @var TestGuessPropBuildableObject $object */
		$object = $this->_factory->applyTo(new TestGuessPropBuildableObject(), $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestGuessPropBuildableObject::class, $object);
		$this->assertTrue($object->bool);
		$this->assertEquals(1, $object->int);
		$this->assertEquals(1.0, $object->float);
		$this->assertEquals('foobar', $object->string);
		$this->assertEquals([true, false], $object->array);
	}
	
	public function testBuildFails() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'array' => [true, false],
		];
		
		$this->expectException(MissingInnerTypeException::class);
		
		$object = $this->_factory->applyTo(new TestGuessPropBuildableObject(), $data, 0, '.', $this->_config);
	}
	
	public function testBuildEmpty() : void
	{
		$this->assertInstanceOf(TestGuessPropBuildableObject::class, $this->_factory->applyTo(new TestGuessPropBuildableObject(), [], 0, '.', $this->_config));
	}
	
	public function testNonExistant() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestGuessPropBuildableObject(), ['toto' => 'foobar'], 0, '.', $this->_config);
	}
	
	public function testNonApplicable() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestGuessPropBuildableObject(), ['bool' => new DateTimeImmutable()], 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$reifier = new Reifier();
		$ensurer = new LooseEnsurer();
		$rclass = new ReflectionClass(TestGuessPropBuildableObject::class);
		$this->_factory = new ObjectFactoryField($reifier, $ensurer, $rclass, new ObjectFactoryIgnore($reifier, $ensurer, $rclass));
		$this->_config = new ReifierConfiguration();
	}
	
}
