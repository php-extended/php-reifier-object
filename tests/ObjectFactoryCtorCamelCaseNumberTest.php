<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class CamelCaseableNumber
{
	
	protected string $field2Name;
	
	public function __construct(string $field2Name)
	{
		$this->field2Name = $field2Name;
	}
	
	public function getField2Name() : string
	{
	return $this->field2Name;
	}
	
}

/**
 * ObjectFactoryCtorCamelCaseNumberTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorCamelCaseNumberTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testItWorks() : void
	{
		$data = [
			'field2_name' => 'value',
		];
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertInstanceOf(CamelCaseableNumber::class, $object);
		$this->assertEquals($object->getField2Name(), 'value');
	}
	
	public function testItWorksVariant() : void
	{
		$data = [
			'field_2_name' => 'value',
		];
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertInstanceOf(CamelCaseableNumber::class, $object);
		$this->assertEquals($object->getField2Name(), 'value');
	}
	
	// this test fails as 2 cannot be camelized
// 	public function testItWorksVariant2() : void
// 	{
// 		$data = [
// 			'field_2name' => 'value',
// 		];
		
// 		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
// 		$this->assertInstanceOf(CamelCaseableNumber::class, $object);
// 		$this->assertEquals($object->getField2Name(), 'value');
// 	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(CamelCaseableNumber::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
