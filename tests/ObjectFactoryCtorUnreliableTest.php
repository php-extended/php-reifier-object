<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class UnreliableCtorObject
{
	
	// not typed intentionally
	protected $_data;
	
	public function __construct($namedData)
	{
		$this->_data = $namedData;
	}
	
	public function getData()
	{
		return $this->_data;
	}
	
}

/**
 * ObjectFactoryCtorUnreliableTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorUnreliableTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testTryInstanciate() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'data' => 'toto',
		];
		
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(UnreliableCtorObject::class));
		$this->_config = new ReifierConfiguration();
		$this->_config->addFieldNameAlias(UnreliableCtorObject::class, 'namedData', 'data');
	}
	
}
