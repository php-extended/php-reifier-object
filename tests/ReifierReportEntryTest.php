<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReifierReport;
use PhpExtended\Reifier\ReifierReportEntry;
use PHPUnit\Framework\TestCase;

/**
 * ReifierReportEntryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierReportEntry
 * @internal
 * @small
 */
class ReifierReportEntryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReifierReportEntry
	 */
	protected ReifierReportEntry $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIndex() : void
	{
		$this->assertEquals(3, $this->_object->getIndex());
	}
	
	public function testGetPath() : void
	{
		$this->assertEquals('.path[to].field', $this->_object->getPath());
	}
	
	public function testGetOffset() : void
	{
		$this->assertEquals(10, $this->_object->getOffset());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('{data: "data"}', $this->_object->getData());
	}
	
	public function testGetClassname() : void
	{
		$this->assertEquals(ReifierReport::class, $this->_object->getClassname());
	}
	
	public function testGetMessage() : void
	{
		$this->assertEquals('Report failed', $this->_object->getMessage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReifierReportEntry(3, '.path[to].field', 10, '{data: "data"}', ReifierReport::class, 'Report failed');
	}
	
}
