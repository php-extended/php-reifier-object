<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParserInterface;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\MissingParserException;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestRecursiveLvThreeObject
{
	
	/**
	 * @var integer
	 */
	protected int $_index;
	
	/**
	 * @param integer $index
	 */
	public function __construct(int $index = 0)
	{
		$this->_index = $index;
	}
	
	/**
	 * @param integer $index
	 */
	public function setIndex(int $index) : void
	{
		$this->_index = $index;
	}
	
	/**
	 * @return integer
	 */
	public function getIndex() : int
	{
		return $this->_index;
	}
	
}


class TestRecursiveLvTwoObject
{
	
	/**
	 * @var TestRecursiveLvThreeObject
	 */
	protected TestRecursiveLvThreeObject $_lv3;
	
	/**
	 * @var Iterator<TestRecursiveLvThreeObject>
	 */
	protected Iterator $_lv3s;
	
	/**
	 * @var array<integer, TestRecursiveLvThreeObject>
	 */
	protected array $_arrlv3s;
	
	/**
	 * Constructor.
	 *
	 * @param TestRecursiveLvThreeObject $lv3
	 * @param Iterator<TestRecursiveLvThreeObject> $lv3s
	 * @param array<integer, TestRecursiveLvThreeObject> $arrlv3s
	 */
	public function __construct(TestRecursiveLvThreeObject $lv3, Iterator $lv3s, array $arrlv3s)
	{
		$this->_lv3 = $lv3;
		$this->_lv3s = $lv3s;
		$this->_arrlv3s = $arrlv3s;
	}
	
	/**
	 * The lv3.
	 *
	 * @return TestRecursiveLvThreeObject
	 */
	public function getLv3() : TestRecursiveLvThreeObject
	{
		return $this->_lv3;
	}
	
	/**
	 * The lv3s.
	 *
	 * @return Iterator<TestRecursiveLvThreeObject>
	 */
	public function getLv3s() : Iterator
	{
		return $this->_lv3s;
	}
	
	/**
	 * The other lv3s.
	 * 
	 * @return array<integer, TestRecursiveLvThreeObject>
	 */
	public function getArrLv3s() : array
	{
		return $this->_arrlv3s;
	}
	
}

class TestRecursiveLvOneObject
{
	
	/**
	 * @var ?TestRecursiveLvTwoObject
	 */
	protected ?TestRecursiveLvTwoObject $_lv2;
	
	/**
	 * @var Iterator<TestRecursiveLvTwoObject>
	 */
	protected Iterator $_lv2s;
	
	/**
	 * @var array<integer, TestRecursiveLvTwoObject>
	 */
	protected array $_arrlv2s;
	
	/**
	 * Constructor.
	 * 
	 * @param ?TestRecursiveLvTwoObject $lv2
	 * @param Iterator<TestRecursiveLvTwoObject> $lv2s
	 * @param array<integer, TestRecursiveLvTwoObject> $arrlv2s
	 */
	public function __construct(?TestRecursiveLvTwoObject $lv2, Iterator $lv2s, array $arrlv2s)
	{
		$this->_lv2 = $lv2;
		$this->_lv2s = $lv2s;
		$this->_arrlv2s = $arrlv2s;
	}
	
	/**
	 * The lv2.
	 * 
	 * @return ?TestRecursiveLvTwoObject
	 */
	public function getLv2() : ?TestRecursiveLvTwoObject
	{
		return $this->_lv2;
	}
	
	/**
	 * The lv2s.
	 * 
	 * @return Iterator<TestRecursiveLvTwoObject>
	 */
	public function getLv2s() : Iterator
	{
		return $this->_lv2s;
	}
	
	/**
	 * @return array<integer, TestRecursiveLvTwoObject>
	 */
	public function getArrlv2s() : array
	{
		return $this->_arrlv2s;
	}
	
}

/**
 * ObjectFactoryRecursiveTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorRecursiveTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected $_config;
	
	public function testBuildObjects() : void
	{
		$data = [
			'lv2' => new TestRecursiveLvTwoObject(
				new TestRecursiveLvThreeObject(1),
				new ArrayIterator([
					new TestRecursiveLvThreeObject(2),
					new TestRecursiveLvThreeObject(3),
				]),
				[
					new TestRecursiveLvThreeObject(4),
					new TestRecursiveLvThreeObject(5),
				],
			),
			'lv2s' => new ArrayIterator([
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(6),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(7),
						new TestRecursiveLvThreeObject(8),
					]),
					[
						new TestRecursiveLvThreeObject(9),
						new TestRecursiveLvThreeObject(10),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(11),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(12),
						new TestRecursiveLvThreeObject(13),
					]),
					[
						new TestRecursiveLvThreeObject(14),
						new TestRecursiveLvThreeObject(15),
					],
				),
			]),
			'arrlv2s' => [
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(16),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(17),
						new TestRecursiveLvThreeObject(18),
					]),
					[
						new TestRecursiveLvThreeObject(19),
						new TestRecursiveLvThreeObject(20),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(21),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(22),
						new TestRecursiveLvThreeObject(23),
					]),
					[
						new TestRecursiveLvThreeObject(24),
						new TestRecursiveLvThreeObject(25),
					],
				),
			],
		];
		
		$expected = new TestRecursiveLvOneObject(
			new TestRecursiveLvTwoObject(
				new TestRecursiveLvThreeObject(1),
				new ArrayIterator([
					new TestRecursiveLvThreeObject(2),
					new TestRecursiveLvThreeObject(3),
				]),
				[
					new TestRecursiveLvThreeObject(4),
					new TestRecursiveLvThreeObject(5),
				],
			),
			new ArrayIterator([
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(6),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(7),
						new TestRecursiveLvThreeObject(8),
					]),
					[
						new TestRecursiveLvThreeObject(9),
						new TestRecursiveLvThreeObject(10),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(11),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(12),
						new TestRecursiveLvThreeObject(13),
					]),
					[
						new TestRecursiveLvThreeObject(14),
						new TestRecursiveLvThreeObject(15),
					],
				),
			]),
			[
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(16),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(17),
						new TestRecursiveLvThreeObject(18),
					]),
					[
						new TestRecursiveLvThreeObject(19),
						new TestRecursiveLvThreeObject(20),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(21),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(22),
						new TestRecursiveLvThreeObject(23),
					]),
					[
						new TestRecursiveLvThreeObject(24),
						new TestRecursiveLvThreeObject(25),
					],
				),
			],
		);
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s', 'arrlv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s', 'arrlv3s'], TestRecursiveLvThreeObject::class);
		$this->assertEquals($expected, $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	public function testBuildArrays() : void
	{
		$data = [
			'lv2' => [
				'lv3' => [
					'index' => 1,
				],
				'lv3s' => [
					[
						'index' => 2,
					],
					[
						'index' => 3,
					],
				],
				'arrlv3s' => [
					[
						'index' => 4,
					],
					[
						'index' => 5,
					],
				],
			],
			'lv2s' => [
				[
					'lv3' => [
						'index' => 6,
					],
					'lv3s' => [
						[
							'index' => 7,
						],
						[
							'index' => 8,
						],
					],
					'arrlv3s' => [
						[
							'index' => 9,
						],
						[
							'index' => 10,
						],
					],
				],
				[
					'lv3' => [
						'index' => 11,
					],
					'lv3s' => [
						[
							'index' => 12,
						],
						[
							'index' => 13,
						],
					],
					'arrlv3s' => [
						[
							'index' => 14,
						],
						[
							'index' => 15,
						],
					],
				],
			],
			'arrlv2s' => [
				[
					'lv3' => [
						'index' => 16,
					],
					'lv3s' => [
						[
							'index' => 17,
						],
						[
							'index' => 18,
						],
					],
					'arrlv3s' => [
						[
							'index' => 19,
						],
						[
							'index' => 20,
						],
					],
				],
				[
					'lv3' => [
						'index' => 21,
					],
					'lv3s' => [
						[
							'index' => 22,
						],
						[
							'index' => 23,
						],
					],
					'arrlv3s' => [
						[
							'index' => 24,
						],
						[
							'index' => 25,
						],
					],
				],
			],
		];
		
		$expected = new TestRecursiveLvOneObject(
			new TestRecursiveLvTwoObject(
				new TestRecursiveLvThreeObject(1),
				new ArrayIterator([
					new TestRecursiveLvThreeObject(2),
					new TestRecursiveLvThreeObject(3),
				]),
				[
					new TestRecursiveLvThreeObject(4),
					new TestRecursiveLvThreeObject(5),
				],
			),
			new ArrayIterator([
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(6),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(7),
						new TestRecursiveLvThreeObject(8),
					]),
					[
						new TestRecursiveLvThreeObject(9),
						new TestRecursiveLvThreeObject(10),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(11),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(12),
						new TestRecursiveLvThreeObject(13),
					]),
					[
						new TestRecursiveLvThreeObject(14),
						new TestRecursiveLvThreeObject(15),
					],
				),
			]),
			[
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(16),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(17),
						new TestRecursiveLvThreeObject(18),
					]),
					[
						new TestRecursiveLvThreeObject(19),
						new TestRecursiveLvThreeObject(20),
					],
				),
				new TestRecursiveLvTwoObject(
					new TestRecursiveLvThreeObject(21),
					new ArrayIterator([
						new TestRecursiveLvThreeObject(22),
						new TestRecursiveLvThreeObject(23),
					]),
					[
						new TestRecursiveLvThreeObject(24),
						new TestRecursiveLvThreeObject(25),
					],
				),
			],
		);
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s', 'arrlv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s', 'arrlv3s'], TestRecursiveLvThreeObject::class);
		$this->assertEquals($expected, $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	public function testWrongTypeProvidedFailed() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'lv2' => new DateTimeImmutable(),
			'lv2s' => new ArrayIterator(),
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testWrongTypeProvidedIgnored() : void
	{
		$data = [
			'lv2' => new DateTimeImmutable(),
			'lv2s' => new ArrayIterator(),
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$this->_config->addFieldAllowedToFail(TestRecursiveLvOneObject::class, 'lv2');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertEquals(new TestRecursiveLvOneObject(null, new ArrayIterator(), []), $object);
	}
	
	public function testWrongTypeProvidedSkippedFail() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'lv2' => new DateTimeImmutable(),
			'lv2s' => [
				'lv3' => new DateTimeImmutable(),
				'lv3s' => new DateTimeImmutable(),
			],
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$this->_config->addFieldAllowedToFail(TestRecursiveLvOneObject::class, 'lv2');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertEquals(new TestRecursiveLvOneObject(null, new ArrayIterator(), []), $object);
	}
	
	public function testWrongTypeProvidedSkippedDirectFail() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'lv2' => [
				'lv3' => new DateTimeImmutable(),
			],
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertEquals(new TestRecursiveLvOneObject(new TestRecursiveLvTwoObject(null, new ArrayIterator(), []), new ArrayIterator(), []), $object);
	}
	
	public function testWrongTypeProvidedSkippedFailParsed() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'arrlv2s' => [
				'failed to parse string',
			],
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['arrlv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setParser(TestRecursiveLvTwoObject::class, new class() extends AbstractParser implements ParserInterface
		{
			public function parse(?string $data) : object
			{
				throw new ParseException(TestRecursiveLvTwoObject::class, null, 0);
			}
		});
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testNoInnerTypeProvided() : void
	{
		$this->expectException(MissingInnerTypeException::class);
		
		$data = [
			'lv2' => [
				'lv3' => [
					'index' => 1,
				],
				'lv3s' => [],
			],
		];
		
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testNotAnObjectProvidedFailed() : void
	{
		$this->expectException(ReificationException::class);
		
		$fpointer = \fopen(__FILE__, 'r');
		\fclose($fpointer);
		
		$data = [
			'lv2' => $fpointer, // closed pointer => gettype = "unknown type"
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testNotAnObjectProvidedIgnored() : void
	{
		$fpointer = \fopen(__FILE__, 'r');
		\fclose($fpointer);
		
		$data = [
			'lv2' => $fpointer, // closed pointer => gettype = "unknown type"
		];
		
		$this->_config->setIterableInnerTypes(TestRecursiveLvOneObject::class, ['lv2s'], TestRecursiveLvTwoObject::class);
		$this->_config->setIterableInnerTypes(TestRecursiveLvTwoObject::class, ['lv3s'], TestRecursiveLvThreeObject::class);
		$this->_config->addFieldAllowedToFail(TestRecursiveLvOneObject::class, 'lv2');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertEquals(new TestRecursiveLvOneObject(null, new ArrayIterator(), []), $object);
	}
	
	public function testNullObjectProvided() : void
	{
		$data = [
			'lv2' => null,
		];
		
		$expected = new TestRecursiveLvOneObject(null, new ArrayIterator(), []);
		
		$this->assertEquals($expected, $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	public function testNoParserProvided() : void
	{
		$this->expectException(MissingParserException::class);
		
		$data = [
			'lv2' => 'string-to-parse',
		];
		
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestRecursiveLvOneObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
