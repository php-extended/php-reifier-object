<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryMethod;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class UnacceptableObject
{
	protected string $_prop;
	
	public function setProp(string $prop) : void
	{
		if(\strlen($prop) < 255)
		{
			throw new InvalidArgumentException('Unacceptable parameter');
		}
		
		$this->_prop = $prop;
	}
	
	public function getProp() : string
	{
		return $this->_prop;
	}
	
}

/**
 * ObjectFactoryUnacceptableTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 * @covers \PhpExtended\Reifier\ObjectFactoryMethod
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryMethodUnacceptableTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryMethod
	 */
	protected ObjectFactoryMethod $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testTryInstanciate() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'prop' => 'toto',
		];
		
		$this->_factory->applyTo(new UnacceptableObject(), $data, 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryMethod(new Reifier(), new LooseEnsurer(), new ReflectionClass(UnacceptableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
