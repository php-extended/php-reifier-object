<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\MissingImplementationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierReport;
use PHPUnit\Framework\TestCase;

interface StringCarrierInterfaceFailed
{
	// nothing to add
}

class StringCarrier implements StringCarrierInterfaceFailed
{
	
	/**
	 * The value.
	 *
	 * @var ?string
	 */
	protected ?string $_value;
	
	/**
	 * Constructor.
	 *
	 * @param ?string $value
	 */
	public function __construct(?string $value)
	{
		$this->_value = $value;
	}
	
	/**
	 * Get value.
	 *
	 * @return ?string
	 */
	public function getString() : ?string
	{
		return $this->_value;
	}
	
}

/**
 * ReifierTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\Reifier
 *
 * @internal
 *
 * @small
 */
class ReifierTest extends TestCase
{
	
	/**
	 * The reifier.
	 * 
	 * @var Reifier
	 */
	protected Reifier $_reifier;
	
	public function testToString() : void
	{
		$object = $this->_reifier;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testBuildSuccess() : void
	{
		$expected1 = new StringCarrier('toto');
		$expected2 = new StringCarrier('toto');
		
		$build1 = $this->_reifier->reify(StringCarrier::class, ['value' => 'toto']);
		$build2 = $this->_reifier->reify(StringCarrier::class, ['value' => 'toto']);
		
		$this->assertEquals($expected1, $build1);
		$this->assertEquals($expected2, $build2);
	}
	
	public function testBuildError() : void
	{
		$this->expectException(MissingImplementationException::class);
		
		$this->_reifier->reify(StringCarrierInterfaceFailed::class, []);
	}
	
	public function testReifyNullable() : void
	{
		$this->assertNull($this->_reifier->reifyNullable(StringCarrier::class, []));
	}
	
	public function testReifyNullableNot() : void
	{
		$this->assertEquals(new StringCarrier('toto'), $this->_reifier->reifyNullable(StringCarrier::class, ['value' => 'toto']));
	}
	
	public function testTryReifySuccess() : void
	{
		$this->assertEquals(new StringCarrier('toto'), $this->_reifier->tryReify(StringCarrier::class, ['value' => 'toto']));
	}
	
	public function testTryReifyFailed() : void
	{
		$report = new ReifierReport(__FILE__);
		$this->assertNull($this->_reifier->tryReify(StringCarrier::class, ['failed' => ''], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testTryReifyNullableSuccess() : void
	{
		$this->assertEquals(new StringCarrier('toto'), $this->_reifier->tryReifyNullable(StringCarrier::class, ['value' => 'toto']));
	}
	
	public function testTryReifyNullableFailed() : void
	{
		$report = new ReifierReport(__FILE__);
		$this->assertNull($this->_reifier->tryReifyNullable(StringCarrier::class, ['failed' => ''], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testReifyAll() : void
	{
		$this->assertEquals([new StringCarrier('toto')], $this->_reifier->reifyAll(StringCarrier::class, [['value' => 'toto']]));
	}
	
	public function testReifyAllNullable() : void
	{
		$this->assertEquals([new StringCarrier('toto')], $this->_reifier->reifyAllNullable(StringCarrier::class, [[], ['value' => 'toto']]));
	}
	
	public function testTryReifyAll() : void
	{
		$report = new ReifierReport(__FILE__);
		$this->assertEquals([new StringCarrier('toto')], $this->_reifier->tryReifyAll(StringCarrier::class, [['failed' => ''], ['value' => 'toto']], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testTryReifyAllNullable() : void
	{
		$report = new ReifierReport(__FILE__);
		$this->assertEquals([new StringCarrier('toto')], $this->_reifier->tryReifyAllNullable(StringCarrier::class, [['failed' => ''], [], ['value' => 'toto']], $report));
		$this->assertEquals(1, $report->count());
	}
	
	public function testReifyIterator() : void
	{
		$count = 0;
		
		foreach($this->_reifier->reifyIterator(StringCarrier::class, new ArrayIterator([['value' => 'toto']])) as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(StringCarrier::class, $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	public function testReifyNullableIterator() : void
	{
		$count = 0;
		
		foreach($this->_reifier->reifyIteratorNullable(StringCarrier::class, new ArrayIterator([[], ['value' => 'toto']])) as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(StringCarrier::class, $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	public function testTryReifyIterator() : void
	{
		$count = 0;
		
		foreach($this->_reifier->tryReifyIterator(StringCarrier::class, new ArrayIterator([['failed' => ''], ['value' => 'toto']])) as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(StringCarrier::class, $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	public function testTryReifyNullableIterator() : void
	{
		$count = 0;
		
		foreach($this->_reifier->tryReifyIteratorNullable(StringCarrier::class, new ArrayIterator([['failed' => ''], [], ['value' => 'toto']])) as $key => $value)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(StringCarrier::class, $value);
			$count++;
		}
		
		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_reifier = new Reifier();
	}
	
}
