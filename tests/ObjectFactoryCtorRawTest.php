<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestRawBuildableObject
{
	
	protected int $_data;
	protected string $_another;
	protected bool $_final;
	
	public function __construct(int $data, string $another, bool $final)
	{
		$this->_data = $data;
		$this->_another = $another;
		$this->_final = $final;
	}
	
	public function getData() : int
	{
	return $this->_data;
	}
	public function getAnother() : string
	{
	return $this->_another;
	}
	public function getFinal() : bool
	{
	return $this->_final;
	}
	
}

/**
 * ObjectFactoryRawTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorRawTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'123',
			'a string',
			'false',
		];
		
		/** @var TestRawBuildableObject $object */
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertEquals(123, $object->getData());
		$this->assertEquals('a string', $object->getAnother());
		$this->assertFalse($object->getFinal());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestRawBuildableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
