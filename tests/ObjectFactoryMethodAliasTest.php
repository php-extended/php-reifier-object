<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryMethod;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class MethodAliasable
{
	
	protected string $fieldname;
	
	public function setFieldName(string $doesntmatter) : MethodAliasable
	{
		$this->fieldname = $doesntmatter;
		
		return $this;
	}
	
	public function getFieldName() : string
	{
	return $this->fieldname;
	}
	
}

/**
 * ObjectFactoryMethodAliasTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 * @covers \PhpExtended\Reifier\ObjectFactoryMethod
 * 
 * @internal
 *
 * @small
 */
class ObjectFactoryMethodAliasTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var ObjectFactoryMethod
	 */
	protected ObjectFactoryMethod $_factory;
	
	/**
	 * The configuration to provide.
	 * 
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testTryInstanciate() : void
	{
		$data = [
			'alias' => 'value',
		];
		$object = $this->_factory->applyTo(new MethodAliasable(), $data, 0, '.', $this->_config);
		$this->assertInstanceOf(MethodAliasable::class, $object);
		$this->assertEquals($object->getFieldname(), 'value');
	}
	
	public function testLookUpSetter() : void
	{
		$this->assertNotNull($this->_factory->lookUpSetter('fieldName', 'fieldName', $this->_config));
	}
	
	public function testLookUpSetterLower() : void
	{
		$this->assertNotNull($this->_factory->lookUpSetter('fieldName', 'FIELDNAME', $this->_config));
	}
	
	public function testLookUpCamelCaseSetter() : void
	{
		$this->assertNull($this->_factory->lookUpCamelCaseSetter('key', $this->_config));
	}
	
	public function testLookUpDashCaseSetter() : void
	{
		$this->assertNull($this->_factory->lookUpDashCaseSetter('key', $this->_config));
	}
	
	public function testLookUpPascalCaseSetter() : void
	{
		$this->assertNull($this->_factory->lookUpPascalCaseSetter('key', $this->_config));
	}
	
	public function testLookUpSnakeCaseSetter() : void
	{
		$this->assertNull($this->_factory->lookUpSnakeCaseSetter('key', $this->_config));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryMethod(new Reifier(), new LooseEnsurer(), new ReflectionClass(MethodAliasable::class));
		$this->_config = new ReifierConfiguration();
		$this->_config->addFieldNameAlias(MethodAliasable::class, 'fieldName', 'alias');
	}
	
}
