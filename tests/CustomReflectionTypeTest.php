<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\CustomReflectionType;
use PHPUnit\Framework\TestCase;

/**
 * CustomReflectionTypeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\CustomReflectionType
 *
 * @internal
 *
 * @small
 */
class CustomReflectionTypeTest extends TestCase
{
	
	/**
	 * The type to test.
	 * 
	 * @var CustomReflectionType
	 */
	protected CustomReflectionType $_type;
	
	public function testAllowsNull() : void
	{
		$this->assertTrue($this->_type->allowsNull());
	}
	
	public function testType() : void
	{
		$this->assertEquals('string', $this->_type->getName());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('?string', $this->_type->__toString());
	}
	
	public function testIsBuiltin() : void
	{
		$this->assertTrue($this->_type->isBuiltin());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_type = new CustomReflectionType('string', true);
	}
	
}
