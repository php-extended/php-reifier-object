<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierIterator;
use PhpExtended\Reifier\ReifierReport;
use PHPUnit\Framework\TestCase;

class ReifiableTryableNullableObject
{
	public string $data;
}

/**
 * ReifierIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierIterator
 * @internal
 * @small
 */
class ReifierIteratorTryableNullableTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReifierIterator
	 */
	protected ReifierIterator $_object;
	
	/**
	 * The report.
	 * 
	 * @var ReifierReport
	 */
	protected ReifierReport $_report;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testReified() : void
	{
		$count = 0;
		$expected = new ReifiableTryableNullableObject();
		$expected->data = 'toto';
		
		foreach($this->_object as $key => $value)
		{
			$this->assertIsInt($key);
			if(0 === $count || 1 === $count)
			{
				$this->assertNull($value);
			}
			else
			{
				$this->assertEquals($expected, $value);
			}
			$count++;
		}
		
		$this->assertEquals(3, $count);
		$this->assertEquals(1, $this->_report->count());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReifierIterator(new Reifier(), ReifiableTryableNullableObject::class, new ArrayIterator([['failed' => 'value'], [], ['data' => 'toto']]), true, true, $this->_report = new ReifierReport(__FILE__));
	}
	
}
