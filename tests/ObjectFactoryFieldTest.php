<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\ObjectFactoryField;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestFieldBuildableObject
{
	
	public static $staticProperty;
	
	public $unknown;
	public $bool;
	public $int;
	public $float;
	public $string;
	public $bools;
	public $ints;
	public $floats;
	public $strings;
	
	public function getBool() : bool
	{
	return $this->bool;
	}
	public function getInt() : int
	{
	return $this->int;
	}
	public function getFloat() : float
	{
	return $this->float;
	}
	public function getString() : string
	{
	return $this->string;
	}
	public function getBools() : array
	{
	return $this->bools;
	}
	public function getInts() : array
	{
	return $this->ints;
	}
	public function getFloats() : array
	{
	return $this->floats;
	}
	public function getStrings() : array
	{
	return $this->strings;
	}
	
}

/**
 * ObjectFactoryFieldTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryField
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryFieldTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryField
	 */
	protected ObjectFactoryField $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->_config->setIterableInnerTypes(TestFieldBuildableObject::class, ['bools'], 'bool');
		$this->_config->setIterableInnerTypes(TestFieldBuildableObject::class, ['ints'], 'int');
		$this->_config->setIterableInnerTypes(TestFieldBuildableObject::class, ['floats'], 'float');
		$this->_config->setIterableInnerTypes(TestFieldBuildableObject::class, ['strings'], 'string');
		/** @var TestFieldBuildableObject $object */
		$object = $this->_factory->applyTo(new TestFieldBuildableObject(), $data, 0, '.', $this->_config);
		// do it twice to force local cached
		$object = $this->_factory->applyTo(new TestFieldBuildableObject(), $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestFieldBuildableObject::class, $object);
		$this->assertTrue($object->getBool());
		$this->assertEquals(1, $object->getInt());
		$this->assertEquals(1.0, $object->getFloat());
		$this->assertEquals('foobar', $object->getString());
		$this->assertEquals([true, false], $object->getBools());
		$this->assertEquals([1, 2], $object->getInts());
		$this->assertEquals([2.0, 3.0], $object->getFloats());
		$this->assertEquals(['foo', 'bar'], $object->getStrings());
	}
	
	public function testBuildFails() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->expectException(MissingInnerTypeException::class);
		
		$object = $this->_factory->applyTo(new TestFieldBuildableObject(), $data, 0, '.', $this->_config);
	}
	
	public function testBuildEmpty() : void
	{
		$this->assertInstanceOf(TestFieldBuildableObject::class, $this->_factory->applyTo(new TestFieldBuildableObject(), [], 0, '.', $this->_config));
	}
	
	public function testNonExistant() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestFieldBuildableObject(), ['toto' => 'foobar'], 0, '.', $this->_config);
	}
	
	public function testNonApplicable() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestFieldBuildableObject(), ['bool' => new DateTimeImmutable()], 0, '.', $this->_config);
	}
	
	public function testUnknown() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestFieldBuildableObject(), ['unknown' => 1], 0, '.', $this->_config);
	}
	
	public function testToNull() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(null, [], 1, '.', $this->_config);
	}
	
	public function testIgnoreKey() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestFieldBuildableObject(), ['nonexistant' => 1], 0, '.', $this->_config);
	}
	
	public function testIgnoreCachedKey() : void
	{
		$this->expectException(ReificationException::class);
		
		try
		{
			$this->_factory->applyTo(new TestFieldBuildableObject(), ['nonexistant' => 1], 0, '.', $this->_config);
		}
		catch(Throwable $exc)
		{ // ...
		}
		
		$this->_factory->applyTo(new TestFieldBuildableObject(), ['nonexistant' => 1], 0, '.', $this->_config);
	}
	
	public function testAlias() : void
	{
		$this->_config->addFieldNameAlias(TestFieldBuildableObject::class, 'float', 'double');
		
		$data = [
			'double' => 3.14,
		];
		
		$this->assertInstanceOf(TestFieldBuildableObject::class, $this->_factory->applyTo(new TestFieldBuildableObject(), $data, 0, '.', $this->_config));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$reifier = new Reifier();
		$ensurer = new LooseEnsurer();
		$rclass = new ReflectionClass(TestFieldBuildableObject::class);
		$this->_factory = new ObjectFactoryField($reifier, $ensurer, $rclass, new ObjectFactoryIgnore($reifier, $ensurer, $rclass));
		$this->_config = new ReifierConfiguration();
	}
	
}
