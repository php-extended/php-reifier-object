<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReifierConfiguration;
use PHPUnit\Framework\TestCase;

// {{{ first bundle

interface FirstInterfaceSuffixInterface
{
	// nothing to add
}

class FirstInterfaceSuffix implements FirstInterfaceSuffixInterface
{
	// nothing to add
}

// }}}


// {{{ second bundle

interface SecondInterfaceSuffixInterface
{
	// nothing to add
}

abstract class AbstractSecondeInterfaceSuffix implements SecondInterfaceSuffixInterface
{
	// nothing to add
}

class SecondInterfaceSuffix extends AbstractSecondeInterfaceSuffix
{
	// nothing to add
}

// }}}


// {{{ third bundle

interface ThirdInterfaceSuffixInterface
{
	// nothing to add
}

interface FourthInterfaceSuffixInterface extends ThirdInterfaceSuffixInterface
{
	// nothing to add
}

class ThirdInterfaceSuffix implements FourthInterfaceSuffixInterface
{
	// nothing to add
}

// }}}

/**
 * ReifierConfigurationTest class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierConfiguration
 *
 * @internal
 *
 * @small
 */
class ReifierConfigurationInterfaceSuffixTest extends TestCase
{
	
	/**
	 * The configuration to test.
	 *
	 * @var ReifierConfiguration
	 */
	protected ReifierConfiguration $_config;
	
	public function testInferImplementationWithInterfaceSuffix() : void
	{
		$this->assertEquals(FirstInterfaceSuffix::class, $this->_config->getImplementation(FirstInterfaceSuffixInterface::class));
	}
	
	public function testInferImplementationWithInterfaceSuffixSecondLevel() : void
	{
		$this->assertEquals(SecondInterfaceSuffix::class, $this->_config->getImplementation(SecondInterfaceSuffixInterface::class));
	}
	
	public function testInferImplementationWithInterfaceSuffixSecondInterfaceLevel() : void
	{
		$this->assertEquals(ThirdInterfaceSuffix::class, $this->_config->getImplementation(ThirdInterfaceSuffixInterface::class));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_config = new ReifierConfiguration();
	}
	
}
