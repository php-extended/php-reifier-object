<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\MissingInnerTypeException;
use PHPUnit\Framework\TestCase;

/**
 * MissingInnerTypeArrayExceptionTest test file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\MissingInnerTypeException
 *
 * @internal
 *
 * @small
 */
class MissingInnerTypeArrayExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var MissingInnerTypeException
	 */
	protected MissingInnerTypeException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals(['id' => 'data'], $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MissingInnerTypeException(['id' => 'data'], 2, MissingInnerTypeException::class, 'failed', '.path', 'message', -1);
	}
	
}
