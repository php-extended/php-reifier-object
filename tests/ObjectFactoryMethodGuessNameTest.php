<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\ObjectFactoryMethod;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestGuessParamBuildableObject
{
	
	protected $_bool;
	protected $_int;
	protected $_float;
	protected $_string;
	protected $_bools;
	protected $_ints;
	protected $_floats;
	protected $_strings;
	
	public function setBool($bool) : void
	{
	$this->_bool = $bool;
	}
	public function setInt($int) : void
	{
	$this->_int = $int;
	}
	public function setFloat($float) : void
	{
	$this->_float = $float;
	}
	public function setString($string) : void
	{
	$this->_string = $string;
	}
	public function setBools($haystack) : void
	{
	$this->_bools = $haystack;
	}
	public function setInts($array) : void
	{
	$this->_ints = $array;
	}
	public function setFloats($list) : void
	{
	$this->_floats = $list;
	}
	public function setStrings($map) : void
	{
	$this->_strings = $map;
	}
	
	public function setFakeField() : void {}
	
	public function getBool()
	{
	return $this->_bool;
	}
	public function getInt()
	{
	return $this->_int;
	}
	public function getFloat()
	{
	return $this->_float;
	}
	public function getString()
	{
	return $this->_string;
	}
	public function getBools()
	{
	return $this->_bools;
	}
	public function getInts()
	{
	return $this->_ints;
	}
	public function getFloats()
	{
	return $this->_floats;
	}
	public function getStrings()
	{
	return $this->_strings;
	}
	
}

/**
 * ObjectFactoryGuessParamTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 * @covers \PhpExtended\Reifier\ObjectFactoryMethod
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryMethodGuessNameTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryMethod
	 */
	protected ObjectFactoryMethod $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->_config->setIterableInnerTypes(TestGuessParamBuildableObject::class, ['bools'], 'bool');
		$this->_config->setIterableInnerTypes(TestGuessParamBuildableObject::class, ['ints'], 'int');
		$this->_config->setIterableInnerTypes(TestGuessParamBuildableObject::class, ['floats'], 'float');
		$this->_config->setIterableInnerTypes(TestGuessParamBuildableObject::class, ['strings'], 'string');
		/** @var TestGuessParamBuildableObject $object */
		$object = $this->_factory->applyTo(new TestGuessParamBuildableObject(), $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestGuessParamBuildableObject::class, $object);
		$this->assertTrue($object->getBool());
		$this->assertEquals(1, $object->getInt());
		$this->assertEquals(1.0, $object->getFloat());
		$this->assertEquals('foobar', $object->getString());
		$this->assertEquals([true, false], $object->getBools());
		$this->assertEquals([1, 2], $object->getInts());
		$this->assertEquals([2.0, 3.0], $object->getFloats());
		$this->assertEquals(['foo', 'bar'], $object->getStrings());
	}
	
	public function testBuildFails() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->expectException(MissingInnerTypeException::class);
		
		$object = $this->_factory->applyTo(new TestGuessParamBuildableObject(), $data, 0, '.', $this->_config);
	}
	
	public function testBuildEmpty() : void
	{
		$this->assertInstanceOf(TestGuessParamBuildableObject::class, $this->_factory->applyTo(new TestGuessParamBuildableObject(), [], 0, '.', $this->_config));
	}
	
	public function testNonExistant() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestGuessParamBuildableObject(), ['toto' => 'foobar'], 0, '.', $this->_config);
	}
	
	public function testNonApplicable() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(new TestGuessParamBuildableObject(), ['bool' => new DateTimeImmutable()], 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$reifier = new Reifier();
		$ensurer = new LooseEnsurer();
		$rclass = new ReflectionClass(TestGuessParamBuildableObject::class);
		$this->_factory = new ObjectFactoryMethod($reifier, $ensurer, $rclass, new ObjectFactoryIgnore($reifier, $ensurer, $rclass));
		$this->_config = new ReifierConfiguration();
	}
	
}
