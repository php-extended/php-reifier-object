<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestNoCtorObject
{
	
	protected array $_data = [];
	
	public function setData(array $data) : TestNoCtorObject
	{
		$this->_data = $data;
		
		return $this;
	}
	
	public function getData() : array
	{
		return $this->_data;
	}
	
}

/**
 * ObjectFactoryCtorNoCtorTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorNoCtorTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildArray() : void
	{
		$data = [
			'data' => [
				'8732728a-0c89-413c-9c70-a5408d7f693e' => 1,
			],
		];
		
		$this->_config->setIterableInnerTypes(TestNoCtorObject::class, ['data'], 'int');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestNoCtorObject::class, $object);
		$this->assertEquals([], $object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestNoCtorObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
