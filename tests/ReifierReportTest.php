<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReifierReport;
use PhpExtended\Reifier\ReifierReportEntry;
use PHPUnit\Framework\TestCase;

/**
 * ReifierReportTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierReport
 * @internal
 * @small
 */
class ReifierReportTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReifierReport
	 */
	protected ReifierReport $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(2, $this->_object->count());
	}
	
	public function testIteration() : void
	{
		foreach($this->_object as $key => $entry)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(ReifierReportEntry::class, $entry);
		}
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals(__FILE__, $this->_object->getSource());
	}
	
	public function testClearAll() : void
	{
		$this->_object->clear();
		$this->assertEquals(0, $this->_object->count());
	}
	
	public function testClearSelect() : void
	{
		$this->_object->clear(ReifierReport::class);
		$this->assertEquals(1, $this->_object->count());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReifierReport(__FILE__, [
			new ReifierReportEntry(10, '.path[to].field', 3, '{data: "data"}', ReifierReport::class, 'Message Failed'),
		]);
		$this->_object->addEntry(new ReifierReportEntry(12, '.path[to].field', 4, '{data: "data"}', ReifierReportEntry::class, 'Message Failed'));
	}
	
}
