<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ObjectFactoryField;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\ObjectFactoryMethod;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestMethodBuildableObject
{
	
	public static function staticNotASetter(bool $bool)
	{
	return $bool;
	}
	
	public $camel_case_property;
	
	protected $_hidden_camel_property;
	
	protected $_bool;
	protected $_int;
	protected $_float;
	protected $_string;
	protected $_bools;
	protected $_ints;
	protected $_floats;
	protected $_strings;
	
	public function __construct($hidden_camel_property)
	{
		$this->_hidden_camel_property = $hidden_camel_property;
	}
	
	public function setAlsoNotASetter(bool $var1, int $var2)
	{
	return $var1 || $var2;
	}
	
	public function setBool(bool $bool) : void
	{
	$this->_bool = $bool;
	}
	public function setInt(int $int) : void
	{
	$this->_int = $int;
	}
	public function setFloat(float $float) : void
	{
	$this->_float = $float;
	}
	public function setString(string $string) : void
	{
	$this->_string = $string;
	}
	public function setBools(array $bools) : void
	{
	$this->_bools = $bools;
	}
	public function setInts(array $ints) : void
	{
	$this->_ints = $ints;
	}
	public function setFloats(array $floats) : void
	{
	$this->_floats = $floats;
	}
	public function setStrings(array $strings) : void
	{
	$this->_strings = $strings;
	}
	
	public function getBool()
	{
	return $this->_bool;
	}
	public function getInt()
	{
	return $this->_int;
	}
	public function getFloat()
	{
	return $this->_float;
	}
	public function getString()
	{
	return $this->_string;
	}
	public function getBools()
	{
	return $this->_bools;
	}
	public function getInts()
	{
	return $this->_ints;
	}
	public function getFloats()
	{
	return $this->_floats;
	}
	public function getStrings()
	{
	return $this->_strings;
	}
	
	public function getCamelCaseProperty() : bool
	{
	return $this->camel_case_property;
	}
	public function getHiddenCamelProperty() : bool
	{
	return $this->_hidden_camel_property;
	}
	
}

/**
 * ObjectFactoryMethodTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 * @covers \PhpExtended\Reifier\ObjectFactoryMethod
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryMethodTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
			'camel_case_property' => true,
			'hidden_camel_property' => true,
		];
		
		$this->_config->setIterableInnerTypes(TestMethodBuildableObject::class, ['bools'], 'bool');
		$this->_config->setIterableInnerTypes(TestMethodBuildableObject::class, ['ints'], 'int');
		$this->_config->setIterableInnerTypes(TestMethodBuildableObject::class, ['floats'], 'float');
		$this->_config->setIterableInnerTypes(TestMethodBuildableObject::class, ['strings'], 'string');
		/** @var TestMethodBuildableObject $object */
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		// double for the inner factory caches
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestMethodBuildableObject::class, $object);
		$this->assertTrue($object->getBool());
		$this->assertEquals(1, $object->getInt());
		$this->assertEquals(1.0, $object->getFloat());
		$this->assertEquals('foobar', $object->getString());
		$this->assertEquals([true, false], $object->getBools());
		$this->assertEquals([1, 2], $object->getInts());
		$this->assertEquals([2.0, 3.0], $object->getFloats());
		$this->assertEquals(['foo', 'bar'], $object->getStrings());
		$this->assertTrue($object->getCamelCaseProperty());
		$this->assertTrue($object->getHiddenCamelProperty());
	}
	
	public function testBuildFails() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->expectException(MissingInnerTypeException::class);
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testBuildEmpty() : void
	{
		$this->assertInstanceOf(TestMethodBuildableObject::class, $this->_factory->applyTo(null, [], 0, '.', $this->_config));
	}
	
	public function testNonExistant() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(null, ['toto' => 'foobar'], 0, '.', $this->_config);
	}
	
	public function testNonApplicable() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(null, ['bool' => new DateTimeImmutable()], 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$reifier = new Reifier();
		$ensurer = new LooseEnsurer();
		$rclass = new ReflectionClass(TestMethodBuildableObject::class);
		$fact4 = new ObjectFactoryIgnore($reifier, $ensurer, $rclass);
		$fact3 = new ObjectFactoryField($reifier, $ensurer, $rclass, $fact4);
		$fact2 = new ObjectFactoryMethod($reifier, $ensurer, $rclass, $fact3);
		$this->_factory = new ObjectFactoryCtor($reifier, $ensurer, $rclass, $fact2);
		$this->_config = new ReifierConfiguration();
	}
	
}
