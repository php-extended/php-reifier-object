<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestArrayObject
{
	
	protected array $_data;
	
	public function __construct(array $data)
	{
		$this->_data = $data;
	}
	
	public function getData() : array
	{
	return $this->_data;
	}
	
}

/**
 * ObjectFactoryArrayTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorArrayTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildArray() : void
	{
		$data = [
			'data' => [
				'8732728a-0c89-413c-9c70-a5408d7f693e' => 1,
				'03acf6f5-7871-4809-9757-89e08958fa60' => 2,
				'45537303-983a-49e3-9595-c678b530b592' => 3,
				'e97e3892-6266-43f9-b3f9-90315d4b5280' => 4,
				'77b5faf5-a120-4d5e-bd77-cb05d7f2a84d' => 5,
				'b1a7ea34-1f29-4a58-8054-3329844cba03' => 6,
				'db830e24-3a51-490c-a8ec-b5987b36a10a' => 7,
				'f55e52b4-2802-459b-9bac-d8eb6b0e1b57' => 8,
				'bd48f8b1-f6ae-4c7e-8cf4-59609fb0ac2b' => 9,
				'43567f9a-c414-41fe-9c84-8b89c1f37a89' => 0,
			],
		];
		
		$this->_config->setIterableInnerTypes(TestArrayObject::class, ['data'], 'int');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestArrayObject::class, $object);
		
		$this->assertEquals($data['data'], $object->getData());
	}
	
// 	public function testMixedArray() : void
// 	{
// 		$data = [
// 			'data' => [
// 				'abc',
// 				123,
// 				['foo' => 'bar'],
// 			],
// 		];
		
// 		$this->_config->setIterableInnerTypes(TestArrayObject::class, ['data'], 'null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>');
// 		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
// 		$this->assertInstanceOf(TestArrayObject::class, $object);
		
// 		$this->assertEquals($data['data'], $object->getData());
// 	}
	
// 	public function testMixedArray2() : void
// 	{
// 		$data = [
// 			'data' => 'toto',
// 		];
		
// 		$this->_config->setIterableInnerType(TestArrayObject::class, 'data', 'null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array>');
// 		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
// 		$this->assertInstanceOf(TestArrayObject::class, $object);
		
// 		$this->assertEquals(['toto'], $object->getData());
// 	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestArrayObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
