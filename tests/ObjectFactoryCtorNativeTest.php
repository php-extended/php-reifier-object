<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\MissingInnerTypeException;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestPrimitiveBuildableObject
{
	
	protected bool $_bool;
	protected int $_int;
	protected float $_float;
	protected string $_string;
	protected array $_bools;
	protected array $_ints;
	protected array $_floats;
	protected array $_strings;
	
	public function __construct(bool $bool, int $int, float $float, string $string, array $bools, array $ints, array $floats, array $strings)
	{
		$this->_bool = $bool;
		$this->_int = $int;
		$this->_float = $float;
		$this->_string = $string;
		$this->_bools = $bools;
		$this->_ints = $ints;
		$this->_floats = $floats;
		$this->_strings = $strings;
	}
	
	public function getBool()
	{
		return $this->_bool;
	}
	
	public function getInt()
	{
		return $this->_int;
	}
	
	public function getFloat()
	{
		return $this->_float;
	}
	
	public function getString()
	{
		return $this->_string;
	}
	
	public function getBools()
	{
		return $this->_bools;
	}
	
	public function getInts()
	{
		return $this->_ints;
	}
	
	public function getFloats()
	{
		return $this->_floats;
	}
	
	public function getStrings()
	{
		return $this->_strings;
	}
	
}

/**
 * ObjectFactoryNativeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorNativeTest extends TestCase
{
	
	/**
	 * The factory to test.
	 * 
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 * 
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testToString() : void
	{
		$object = $this->_factory;
		$this->assertEquals(\get_class($object).'['.TestPrimitiveBuildableObject::class.']@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->_config->setIterableInnerTypes(TestPrimitiveBuildableObject::class, ['bools'], 'bool');
		$this->_config->setIterableInnerTypes(TestPrimitiveBuildableObject::class, ['ints'], 'int');
		$this->_config->setIterableInnerTypes(TestPrimitiveBuildableObject::class, ['floats'], 'float');
		$this->_config->setIterableInnerTypes(TestPrimitiveBuildableObject::class, ['strings'], 'string');
		/** @var TestPrimitiveBuildableObject $object */
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestPrimitiveBuildableObject::class, $object);
		$this->assertTrue($object->getBool());
		$this->assertEquals(1, $object->getInt());
		$this->assertEquals(1.0, $object->getFloat());
		$this->assertEquals('foobar', $object->getString());
		$this->assertEquals([true, false], $object->getBools());
		$this->assertEquals([1, 2], $object->getInts());
		$this->assertEquals([2.0, 3.0], $object->getFloats());
		$this->assertEquals(['foo', 'bar'], $object->getStrings());
	}
	
	public function testBuildFails() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
		];
		
		$this->expectException(MissingInnerTypeException::class);
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testBuildEmpty() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_factory->applyTo(null, [], 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestPrimitiveBuildableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
