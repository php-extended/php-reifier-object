<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReifierConfiguration;
use PHPUnit\Framework\TestCase;

// {{{ first bundle

interface IName
{
	// nothing to add
}

class Name implements IName
{
	// nothing to add
}

// }}}


// {{{ second bundle

interface ISecondName
{
	// nothing to add
}

abstract class AbstractSecondName implements ISecondName
{
	// nothing to add
}

class SecondName extends AbstractSecondName
{
	// nothing to add
}

// }}}


// {{{ third bundle

interface ISecondLevel
{
	// nothing to add
}

interface IThirdLevel extends ISecondLevel
{
	// nothing to add
}

class SecondLevel implements IThirdLevel
{
	// nothing to add
}

// }}}

/**
 * ReifierConfigurationTest class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReifierConfiguration
 *
 * @internal
 *
 * @small
 */
class ReifierConfigurationIPrefixTest extends TestCase
{
	
	/**
	 * The configuration to test.
	 *
	 * @var ReifierConfiguration
	 */
	protected ReifierConfiguration $_config;
	
	public function testInferImplementationWithIPrefix() : void
	{
		$this->assertEquals(Name::class, $this->_config->getImplementation(IName::class));
	}
	
	public function testInferImplementationWithIPrefixSecondLevel() : void
	{
		$this->assertEquals(SecondName::class, $this->_config->getImplementation(ISecondName::class));
	}
	
	public function testInferImplementationWithIPrefixSecondInterfaceLevel() : void
	{
		$this->assertEquals(SecondLevel::class, $this->_config->getImplementation(ISecondLevel::class));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_config = new ReifierConfiguration();
	}
	
}
