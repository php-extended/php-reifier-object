<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class DateTimeHolder
{
	
	/**
	 * The value.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_datetime;
	
	/**
	 * Constructor.
	 * 
	 * @param ?DateTimeInterface $dti
	 */
	public function __construct(?DateTimeInterface $dti)
	{
		$this->_datetime = $dti;
	}
	
	/**
	 * Get value.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateTime() : ?DateTimeInterface
	{
		return $this->_datetime;
	}
	
}

/**
 * ObjectFactoryDateTimeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorDateTimeTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildEmpty() : void
	{
		$this->assertEquals(new DateTimeHolder(null), $this->_factory->applyTo(null, [], 0, '.', $this->_config));
	}
	
	public function testBuildNull() : void
	{
		$this->assertEquals(new DateTimeHolder(null), $this->_factory->applyTo(null, ['dti' => null], 0, '.', $this->_config));
	}
	
	public function testBuildObject() : void
	{
		$dti = new DateTimeImmutable();
		$this->assertEquals(new DateTimeHolder($dti), $this->_factory->applyTo(null, ['dti' => $dti], 0, '.', $this->_config));
	}
	
	public function testBuildStdString() : void
	{
		$dti = new DateTimeImmutable();
		// reset microseconds
		$dti = $dti->setTime((int) $dti->format('h'), (int) $dti->format('m'), (int) $dti->format('s'), 0);
		$expected = new DateTimeHolder($dti);
		$data = [
			'dti' => $dti->format(DateTime::RFC3339),
		];
		
		$this->assertEquals($expected, $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	public function testBuildCustomStringFailed() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'dti' => '2000,01,01',
		];
		
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	public function testBuildCustomStringIgnored() : void
	{
		$data = [
			'dti' => '2000,01,01',
		];
		
		$this->_config->addFieldAllowedToFail(DateTimeHolder::class, 'dti');
		$this->assertEquals(new DateTimeHolder(null), $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	public function testBuildCustomFormat() : void
	{
		$dt = DateTimeImmutable::createFromFormat('!Y,m,d', '2000,01,01');
		
		$this->_config->addDateTimeFormats(DateTimeHolder::class, ['dti'], ['!Y,m,d']);
		
		$data = [
			'dti' => $dt->format('Y,m,d'),
		];
		
		$this->assertEquals(new DateTimeHolder($dt), $this->_factory->applyTo(null, $data, 0, '.', $this->_config));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(DateTimeHolder::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
