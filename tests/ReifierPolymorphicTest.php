<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\MissingImplementationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PHPUnit\Framework\TestCase;

interface ContenantInterface
{
	// nothing to add
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class ContenantOneTest implements ContenantInterface
{
	public string $type;
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class ContenantTwoTest implements ContenantInterface
{
	public string $type;
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class ContainerTest
{
	
	/**
	 * @var ContenantInterface
	 */
	protected ContenantInterface $c1;
	
	/**
	 * @var ContenantInterface
	 */
	protected ContenantInterface $c2;
	
	public function __construct(ContenantInterface $c1, ContenantInterface $c2)
	{
		$this->c1 = $c1;
		$this->c2 = $c2;
	}
	
	public function getC1() : ContenantInterface
	{
		return $this->c1;
	}
	
	public function getC2() : ContenantInterface
	{
		return $this->c2;
	}
	
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class MultiContainerTest
{	
	/**
	 * @var array<integer, ContenantInterface>
	 */
	protected $_cs;
	
	/**
	 * @param array<integer, ContenantInterface> $cs
	 */
	public function __construct(array $cs)
	{
		$this->_cs = $cs;
	}
	
	/**
	 * @return array<integer, ContenantInterface>
	 */
	public function getCs() : array
	{
		return $this->_cs;
	}
	
}

/**
 * ReifierPolymorphicTest class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\Reifier
 *
 * @internal
 *
 * @small
 */
class ReifierPolymorphicTest extends TestCase
{
	
	/**
	 * The reifier.
	 *
	 * @var Reifier
	 */
	protected Reifier $_reifier;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'c1' => [
				'type' => 'c1',
			],
			'c2' => [
				'type' => 'c2',
			],
		];
		
		$config = new ReifierConfiguration();
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c1', ContenantOneTest::class);
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c2', ContenantTwoTest::class);
		$this->_reifier->setConfiguration($config);
		
		$build = $this->_reifier->reify(ContainerTest::class, $data);
		
		$this->assertInstanceOf(ContainerTest::class, $build);
		$this->assertInstanceOf(ContenantOneTest::class, $build->getC1());
		$this->assertInstanceOf(ContenantTwoTest::class, $build->getC2());
	}
	
	public function testBuildMultiSuccess() : void
	{
		$data = [
			'cs' => [
				[
					'type' => 'c1',
				],
				[
					'type' => 'c2',
				],
			],
		];
		
		$config = new ReifierConfiguration();
		$config->setIterableInnerTypes(MultiContainerTest::class, ['cs'], ContenantInterface::class);
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c1', ContenantOneTest::class);
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c2', ContenantTwoTest::class);
		$this->_reifier->setConfiguration($config);
		
		$build = $this->_reifier->reify(MultiContainerTest::class, $data);
		
		$this->assertInstanceOf(MultiContainerTest::class, $build);
		
		foreach($build->getCs() as $k => $cont)
		{
			if(0 === $k)
			{
				$this->assertInstanceOf(ContenantOneTest::class, $cont);
			}
			else
			{
				$this->assertInstanceOf(ContenantTwoTest::class, $cont);
			}
		}
		$this->assertEquals(1, $k);
	}
	
	public function testBuildFailed() : void
	{
		$this->expectException(MissingImplementationException::class);
		
		$data = [
			'c1' => [
				'type' => 'c1',
			],
			'c2' => [
				'type' => 'c2',
			],
		];
		
		$config = new ReifierConfiguration();
		$config->addPolymorphism(ContenantInterface::class, 0, 'toto', 'value', ContenantInterface::class);
		$this->_reifier->setConfiguration($config);
		
		$this->_reifier->reify(ContainerTest::class, $data);
	}
	
	public function testBuildFailed2() : void
	{
		$this->expectException(MissingImplementationException::class);
		
		$data = [
			'c1' => [
				'type' => 'c1',
			],
			'c2' => [
				'type' => 'c2',
			],
		];
		
		$config = new ReifierConfiguration();
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'value', ContenantInterface::class);
		$this->_reifier->setConfiguration($config);
		
		$this->_reifier->reify(ContainerTest::class, $data);
	}
	
	public function testBuildFailed3() : void
	{
		$this->expectException(MissingImplementationException::class);
		
		$data = [
			'c1' => [
				'type' => ['not a real class-string'],
			],
			'c2' => [
				'type' => 'c2',
			],
		];
		
		$config = new ReifierConfiguration();
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c1', ContenantOneTest::class);
		$config->addPolymorphism(ContenantInterface::class, 0, 'type', 'c2', ContenantTwoTest::class);
		$this->_reifier->setConfiguration($config);
		
		$this->_reifier->reify(ContainerTest::class, $data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_reifier = new Reifier();
	}
	
}
