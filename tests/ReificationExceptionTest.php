<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReificationException;
use PHPUnit\Framework\TestCase;

/**
 * ReificationExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReificationException
 *
 * @internal
 *
 * @small
 */
class ReificationExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReificationException
	 */
	protected ReificationException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetDepths() : void
	{
		$this->assertEquals(1, $this->_object->getDepths());
	}
	
	public function testGetData() : void
	{
		$this->assertNull($this->_object->getData());
	}
	
	public function testGetExpectedClass() : void
	{
		$this->assertEquals(ReificationException::class, $this->_object->getExpectedClass());
	}
	
	public function testGetFailedAttribute() : void
	{
		$this->assertEquals('failed', $this->_object->getFailedAttribute());
	}
	
	public function testGetPath() : void
	{
		$this->assertEquals('.path', $this->_object->getPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReificationException(null, 1, ReificationException::class, 'failed', '.path');
	}
	
}
