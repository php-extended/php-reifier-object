<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestMatrixBuildableObject
{
	
	/**
	 * @var array<integer, array<integer, array<integer, integer>>>
	 */
	protected array $m3d;
	
	/**
	 * @param array<integer, array<integer, array<integer, integer>>> $m3d
	 * @param ?array $other
	 */
	public function __construct(array $m3d, ?array $other)
	{
		$this->m3d = $m3d;
	}
	
	/**
	 * @return array<integer, array<integer, array<integer, integer>>>
	 */
	public function get3dm() : array
	{
		return $this->m3d;
	}
	
}

/**
 * ObjectFactoryMatrixTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorMatrixTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$matrix = [
			[
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 9],
			],
			[
				[11, 12, 13],
				[14, 15, 16],
				[17, 18, 19],
			],
		];
		
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[]'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[][]'], 'int');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['other'], 'string');
		/** @var TestMatrixBuildableObject $object */
		$object = $this->_factory->applyTo(null, ['m3d' => $matrix, 'other' => null], 0, '.', $this->_config);
		$this->assertInstanceOf(TestMatrixBuildableObject::class, $object);
		$this->assertEquals($matrix, $object->get3dm());
	}
	
	public function testBuildFailed() : void
	{
		$this->expectException(ReificationException::class);
		
		$matrix = [
			new DateTimeImmutable(),
		];
		
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[]'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[][]'], 'int');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['other'], 'string');
		$this->_factory->applyTo(null, ['m3d' => $matrix, 'other' => null], 0, '.', $this->_config);
	}
	
	public function testBuildIgnored() : void
	{
		$matrix = [
			new DateTimeImmutable(),
		];
		
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[]'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[][]'], 'int');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['other'], 'string');
		$this->_config->addFieldAllowedToFail(TestMatrixBuildableObject::class, 'm3d');
		/** @var TestMatrixBuildableObject $object */
		$object = $this->_factory->applyTo(null, ['m3d' => $matrix, 'other' => null], 0, '.', $this->_config);
		$this->assertEquals(new TestMatrixBuildableObject([], []), $object);
	}
	
	public function testBuildSkipped() : void
	{
		$matrix = [
			[
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 9],
			],
			null,
			[
				[11, 12, 13],
				[14, 15, 16],
				[17, 18, 19],
			],
		];
		
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[]'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[][]'], 'int');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['other'], 'string');
		/** @var TestMatrixBuildableObject $object */
		$object = $this->_factory->applyTo(null, ['m3d' => $matrix, 'other' => null], 0, '.', $this->_config);
		$this->assertInstanceOf(TestMatrixBuildableObject::class, $object);
		$expected = [
			[
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 9],
			],
			[],
			[
				[11, 12, 13],
				[14, 15, 16],
				[17, 18, 19],
			],
		];
		$this->assertEquals($expected, $object->get3dm());
	}
	
	public function testBuildSkippedObject() : void
	{
		$matrix = [
			[
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 9],
			],
			new DateTimeImmutable(),
			[
				[11, 12, 13],
				[14, 15, 16],
				[17, 18, 19],
			],
		];
		
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[]'], 'array');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['m3d[][]'], 'int');
		$this->_config->setIterableInnerTypes(TestMatrixBuildableObject::class, ['other'], 'string');
		$this->_config->addFieldAllowedToFail(TestMatrixBuildableObject::class, 'm3d[]');
		/** @var TestMatrixBuildableObject $object */
		$object = $this->_factory->applyTo(null, ['m3d' => $matrix, 'other' => null], 0, '.', $this->_config);
		$this->assertInstanceOf(TestMatrixBuildableObject::class, $object);
		$expected = [
			0 => [
				[1, 2, 3],
				[4, 5, 6],
				[7, 8, 9],
			],
			2 => [
				[11, 12, 13],
				[14, 15, 16],
				[17, 18, 19],
			],
		];
		$this->assertEquals($expected, $object->get3dm());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestMatrixBuildableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
