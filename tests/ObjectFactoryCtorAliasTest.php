<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class Aliasable
{
	
	protected string $fieldname;
	
	public function __construct(string $fieldname)
	{
		$this->fieldname = $fieldname;
	}
	
	public function getFieldname() : string
	{
	return $this->fieldname;
	}
	
}

/**
 * ObjectFactoryAliasTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorAliasTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testTryInstanciate() : void
	{
		$data = [
			'alias' => 'value',
		];
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertInstanceOf(Aliasable::class, $object);
		$this->assertEquals($object->getFieldname(), 'value');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(Aliasable::class));
		$this->_config = new ReifierConfiguration();
		$this->_config->addFieldNameAlias(Aliasable::class, 'fieldname', 'alias');
	}
	
}
