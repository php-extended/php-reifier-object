<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PHPUnit\Framework\TestCase;

class IgnorableObject
{
	// nothing
}

/**
 * ObjectFactoryIgnoreTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryIgnore
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryIgnoreTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ObjectFactoryIgnore
	 */
	protected ObjectFactoryIgnore $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'['.IgnorableObject::class.']@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testApplyToNull() : void
	{
		$this->expectException(ReificationException::class);
		
		$this->_object->applyTo(null, [], 1, '.', new ReifierConfiguration());
	}
	
	public function testSuccess() : void
	{
		$this->assertInstanceOf(IgnorableObject::class, $this->_object->applyTo(new IgnorableObject(), [], 1, '.', new ReifierConfiguration()));
	}
	
	public function testFailed() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'nonexistant' => 'nonvalue',
		];
		
		$this->_object->applyTo(new IgnorableObject(), $data, 1, '.', new ReifierConfiguration());
	}
	
	public function testIgnored() : void
	{
		$config = new ReifierConfiguration();
		$config->addIgnoreExcessField(IgnorableObject::class, 'ignorablefield');
		
		$data = [
			'ignorablefield' => 'ignorablevalue',
		];
		
		$this->assertInstanceOf(IgnorableObject::class, $this->_object->applyTo(new IgnorableObject(), $data, 1, '.', $config));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ObjectFactoryIgnore(new Reifier(), new LooseEnsurer(), new ReflectionClass(IgnorableObject::class));
	}
	
}
