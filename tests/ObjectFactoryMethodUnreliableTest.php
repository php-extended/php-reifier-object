<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryMethod;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class UnreliableObject
{
	// no type declared voluntarily
	protected $_prop;
	
	public function setProp($prop) : void
	{
		$this->_prop = $prop;
	}
	
	public function getProp()
	{
		return $this->_prop;
	}
	
}

/**
 * ObjectFactoryUnreliableTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 * @covers \PhpExtended\Reifier\ObjectFactoryMethod
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryMethodUnreliableTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryMethod
	 */
	protected ObjectFactoryMethod $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testTryInstanciate() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'prop' => true,
		];
		
		$this->_factory->applyTo(new UnreliableObject(), $data, 0, '.', $this->_config);
	}
	
	public function testNoAvailableObject() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'prop' => true,
		];
		
		// null triggers exception
		$this->_factory->applyTo(null, $data, 0, '.', $this->_config);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryMethod(new Reifier(), new LooseEnsurer(), new ReflectionClass(UnreliableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
