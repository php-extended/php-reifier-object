<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryField;
use PhpExtended\Reifier\ObjectFactoryIgnore;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestPrototypeObject
{
	// nothing to add
}

class TestTypedPropBuildableObject
{
	
	public bool $field1;
	public int $field2;
	public float $field3;
	public string $field4;
	public array $field5;
	public DateTimeInterface $field6;
	public DateTimeImmutable $field7;
	public Iterator $field8;
	public TestPrototypeObject $field9;
	
}

/**
 * ObjectFactoryGuessPropTest class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryField
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryFieldTypedTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryField
	 */
	protected ObjectFactoryField $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'field1' => true,
			'field2' => 1,
			'field3' => 1.0,
			'field4' => 'foobar',
			'field5' => [true, false],
			'field6' => '2000-01-01',
			'field7' => '2001-01-01',
			'field8' => [true, false],
			'field9' => [],
		];
		
		$this->_config->setIterableInnerTypes(TestTypedPropBuildableObject::class, ['field5', 'field8'], 'bool');
		
		/** @var TestTypedPropBuildableObject $object */
		$object = $this->_factory->applyTo(new TestTypedPropBuildableObject(), $data, 0, '.', $this->_config);
		$this->assertInstanceOf(TestTypedPropBuildableObject::class, $object);
		$this->assertTrue($object->field1);
		$this->assertEquals(1, $object->field2);
		$this->assertEquals(1.0, $object->field3);
		$this->assertEquals('foobar', $object->field4);
		$this->assertEquals([true, false], $object->field5);
		$this->assertEquals('2000-01-01', $object->field6->format('Y-m-d'));
		$this->assertEquals('2001-01-01', $object->field7->format('Y-m-d'));
		$this->assertEquals(new ArrayIterator([true, false]), $object->field8);
		$this->assertEquals(new TestPrototypeObject(), $object->field9);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$reifier = new Reifier();
		$ensurer = new LooseEnsurer();
		$rclass = new ReflectionClass(TestTypedPropBuildableObject::class);
		$this->_factory = new ObjectFactoryField($reifier, $ensurer, $rclass, new ObjectFactoryIgnore($reifier, $ensurer, $rclass));
		$this->_config = new ReifierConfiguration();
	}
}
