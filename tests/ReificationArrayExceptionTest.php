<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\ReificationException;
use PHPUnit\Framework\TestCase;

/**
 * ReificationArrayExceptionTest test file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ReificationException
 *
 * @internal
 *
 * @small
 */
class ReificationArrayExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var ReificationException
	 */
	protected ReificationException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals(['id' => 'data'], $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ReificationException(['id' => 'data'], 1, ReificationException::class, 'failed', '.path', 'message', -1);
	}
	
}
