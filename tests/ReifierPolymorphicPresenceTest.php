<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PHPUnit\Framework\TestCase;

interface ContenantPresenceInterface
{
	// nothing to add
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class ContenantPresenceOneTest implements ContenantPresenceInterface
{
	public string $type1;
}

/**
 * @internal
 *
 * @small
 * @coversNothing
 */
class ContenantPresenceTwoTest implements ContenantPresenceInterface
{
	public string $type2;
}


/**
 * ReifierPolymorphicTest class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\Reifier
 *
 * @internal
 *
 * @small
 */
class ReifierPolymorphicPresenceTest extends TestCase
{
	
	/**
	 * The reifier.
	 *
	 * @var Reifier
	 */
	protected Reifier $_reifier;
	
	public function testBuildSuccess() : void
	{
		$data1 = [
			'type1' => 'c1',
		];
		
		$data2 = [
			'type2' => 'c2',
		];
		
		$config = new ReifierConfiguration();
		$config->addPolymorphismPresence(ContenantPresenceInterface::class, 0, 'type1', ContenantPresenceOneTest::class);
		$config->addPolymorphismPresence(ContenantPresenceInterface::class, 0, 'type2', ContenantPresenceTwoTest::class);
		$this->_reifier->setConfiguration($config);
		
		$build = $this->_reifier->reify(ContenantPresenceInterface::class, $data1);
		$this->assertInstanceOf(ContenantPresenceOneTest::class, $build);
		
		$build = $this->_reifier->reify(ContenantPresenceInterface::class, $data2);
		$this->assertInstanceOf(ContenantPresenceTwoTest::class, $build);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_reifier = new Reifier();
	}
	
}
