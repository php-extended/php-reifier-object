<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Reifier\MissingImplementationException;
use PHPUnit\Framework\TestCase;

/**
 * MissingImplementationExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\MissingImplementationException
 *
 * @internal
 *
 * @small
 */
class MissingImplementationExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MissingImplementationException
	 */
	protected MissingImplementationException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetDepths() : void
	{
		$this->assertEquals(2, $this->_object->getDepths());
	}
	
	public function testGetData() : void
	{
		$this->assertNull($this->_object->getData());
	}
	
	public function testGetExpectedClass() : void
	{
		$this->assertEquals(MissingImplementationException::class, $this->_object->getExpectedClass());
	}
	
	public function testGetFailedAttribute() : void
	{
		$this->assertEquals('failed', $this->_object->getFailedAttribute());
	}
	
	public function testGetPath() : void
	{
		$this->assertEquals('.path', $this->_object->getPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MissingImplementationException(null, 2, MissingImplementationException::class, 'failed', '.path');
	}
	
}
