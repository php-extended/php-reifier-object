<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class TestDefaultNotNullableObject
{
	
	public function __construct(
		bool $bool = true,
		int $int = 1,
		float $float = 1.0,
		string $string = 'str',
		array $bools = [true, false],
		array $ints = [1, 2],
		array $floats = [2.0, 3.0],
		array $strings = ['foo', 'bar'],
		?DateTimeInterface $interface = null,
		?DateTimeImmutable $object = null,
		iterable $iterable = []
	) {
		// nothing to do
	}
	
}

/**
 * ObjectFactoryNotNullableTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorNotNullableTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testBuildSuccess() : void
	{
		$data = [
			'bool' => true,
			'int' => 1,
			'float' => 1.0,
			'string' => 'foobar',
			'bools' => [true, false],
			'ints' => [1, 2],
			'floats' => [2.0, 3.0],
			'strings' => ['foo', 'bar'],
			'interface' => new DateTimeImmutable(),
			'object' => new DateTimeImmutable(),
			'iterable' => ['a string'],
		];
		
		$this->_config->setIterableInnerTypes(TestDefaultNotNullableObject::class, ['bools'], 'bool');
		$this->_config->setIterableInnerTypes(TestDefaultNotNullableObject::class, ['ints'], 'int');
		$this->_config->setIterableInnerTypes(TestDefaultNotNullableObject::class, ['floats'], 'float');
		$this->_config->setIterableInnerTypes(TestDefaultNotNullableObject::class, ['strings', 'iterable'], 'string');
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertInstanceOf(TestDefaultNotNullableObject::class, $object);
	}
	
	public function testBuildEmpty() : void
	{
		$object = $this->_factory->applyTo(null, [], 0, '.', $this->_config);
		
		$this->assertInstanceOf(TestDefaultNotNullableObject::class, $object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(TestDefaultNotNullableObject::class));
		$this->_config = new ReifierConfiguration();
	}
	
}
