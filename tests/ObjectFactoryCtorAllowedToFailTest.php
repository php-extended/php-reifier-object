<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ensurer\LooseEnsurer;
use PhpExtended\Reifier\ObjectFactoryCtor;
use PhpExtended\Reifier\ReificationException;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierConfiguration;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PHPUnit\Framework\TestCase;

class FailableObject
{
	public function __construct(string $data)
	{
		// nothing to do
	}
	
}

class AllowedToFailObject
{
	
	protected ?FailableObject $fieldName;
	
	public function __construct(?FailableObject $failableObject)
	{
		$this->fieldName = $failableObject;
	}
	
	public function getFieldName() : ?FailableObject
	{
		return $this->fieldName;
	}
	
}

/**
 * ObjectFactoryCtorAllowedToFailTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Reifier\ObjectFactoryCtor
 * @covers \PhpExtended\Reifier\ObjectFactoryLink
 *
 * @internal
 *
 * @small
 */
class ObjectFactoryCtorAllowedToFailTest extends TestCase
{
	
	/**
	 * The factory to test.
	 *
	 * @var ObjectFactoryCtor
	 */
	protected ObjectFactoryCtor $_factory;
	
	/**
	 * The configuration to provide.
	 *
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	public function testItFails() : void
	{
		$this->expectException(ReificationException::class);
		
		$data = [
			'object' => [
				'data' => new DateTimeImmutable(),
			],
		];
		
		$object = $this->_factory->applyTo(null, $data, 0, '.', $this->_config);
		
		$this->assertInstanceOf(AllowedToFailObject::class, $object);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_factory = new ObjectFactoryCtor(new Reifier(), new LooseEnsurer(), new ReflectionClass(AllowedToFailObject::class));
		$this->_config = new ReifierConfiguration();
		$this->_config->addFieldNameAlias(AllowedToFailObject::class, 'failableObject', 'object');
	}
	
}
